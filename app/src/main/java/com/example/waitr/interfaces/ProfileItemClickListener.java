package com.example.waitr.interfaces;

public interface ProfileItemClickListener {

    void clickPersonalInfo();
    void clickSettings();
    void clickSupport();
    void clickPrivacyPolicy();
    void clickNotifications();
    void clickBookingHistory();
    void clickAppointments();
    void clickAboutUs();
    void clickLogout();
}
