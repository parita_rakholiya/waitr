package com.example.waitr.loader;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.waitr.R;


public class Loader {

    static String loaderTag = "ProgressBar";

    public static void show(Activity activity) {
        if (isExist(activity)) { return; }

        ViewGroup layout = (ViewGroup) activity.findViewById(android.R.id.content).getRootView();
        ProgressBar mProgressBar = new ProgressBar(activity);
        mProgressBar.setIndeterminate(false);
        mProgressBar.setIndeterminateDrawable(activity.getResources().getDrawable(R.drawable.custom_loader_gradient)); // can change gradient color from custom drawable file
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        RelativeLayout relativeLayout = new RelativeLayout(activity);
        relativeLayout.setGravity(Gravity.CENTER);
        relativeLayout.setTag(loaderTag);
        relativeLayout.addView(mProgressBar);
        layout.addView(relativeLayout, params);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE); // disable user intrection
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public static void hide(Activity activity) {
        if(activity!=null){

            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            ViewGroup viewGroup = (ViewGroup) activity.findViewById(android.R.id.content).getRootView();
            int count = viewGroup.getChildCount();
            for (int i = 0; i < count; i++) {
                View view = viewGroup.getChildAt(i);
                if (view.getTag() != null) {
                    if (view.getTag().equals(loaderTag)) {
                        viewGroup.removeView(view);
                        break;
                    }
                }
            }
        }

    }

    public static Boolean isExist(Activity activity) {
        ViewGroup viewGroup = (ViewGroup) activity.findViewById(android.R.id.content).getRootView();
        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = viewGroup.getChildAt(i);
            if (view.getTag() != null) {
                if (view.getTag().equals(loaderTag)) {
                    return true;
                }
            }
        }
        return false;
    }
}