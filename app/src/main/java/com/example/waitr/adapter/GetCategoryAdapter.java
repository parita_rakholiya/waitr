package com.example.waitr.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.waitr.R;
import com.example.waitr.activity.HomeScreenActivity;
import com.example.waitr.databinding.ItemGetCategoryBinding;
import com.example.waitr.fragment.home.CategoryWiseCompanyListFragment;
import com.example.waitr.model.GetCategories;
import com.example.waitr.utils.ViewControll;

import java.util.ArrayList;
import java.util.List;

public class GetCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<GetCategories.Data> categoryList = new ArrayList<>();
    private Activity activity;

    public GetCategoryAdapter(Activity activity, List<GetCategories.Data> categoryList) {
        this.categoryList = categoryList;
        this.activity = activity;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemGetCategoryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_get_category, parent, false);

        return new RecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        GetCategories.Data itemData = categoryList.get(position);
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;
        viewHolder.bindItem(itemData,position);

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }


    private class RecyclerViewViewHolder extends RecyclerView.ViewHolder {

        ItemGetCategoryBinding binding;

       public RecyclerViewViewHolder(ItemGetCategoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindItem(GetCategories.Data itemData,int position) {
            binding.txtCategory.setText(itemData.getCategory());
            binding.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((HomeScreenActivity)activity).homeScreenBinding.toolbar.setVisibility(View.GONE);
                    ViewControll.loadSubFragment(activity,
                            CategoryWiseCompanyListFragment.newInstance(itemData),
                            "CategoryWiseCompanyListFragment",true);
                }
            });
//            Glide.with(activity).load(APIConstant.IMAGE_BASE_URL+itemData.getImage())
//                    .override(500,500)
//                    .placeholder(R.drawable.app_icon)
//                    .into(binding.img1);
//            Glide.with(activity).load(APIConstant.IMAGE_BASE_URL+itemData.getImage())
//                    .override(500,500)
//                    .placeholder(R.drawable.app_icon)
//                    .into(binding.img2);

            binding.executePendingBindings();

        }
    }

}
