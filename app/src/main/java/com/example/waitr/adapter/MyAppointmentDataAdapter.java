package com.example.waitr.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.waitr.R;
import com.example.waitr.databinding.ItemMyAppointmentBinding;
import com.example.waitr.databinding.ItemProfileDataBinding;
import com.example.waitr.interfaces.ProfileItemClickListener;
import com.example.waitr.model.MyAppointment;
import com.example.waitr.utils.Constants;
import com.example.waitr.utils.ViewControll;
import com.example.waitr.viewmodel.ProfileScreenItemData;

import java.util.ArrayList;
import java.util.List;

public class MyAppointmentDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MyAppointment.Data> myAppointmentList = new ArrayList<>();
    private List<Integer> lightColorList = new ArrayList<>();
    private List<Integer> darkColorList = new ArrayList<>();
    private Activity activity;
    private int itemColorCount = 0;
    private String initialMonthOfList = "";

    public MyAppointmentDataAdapter(Activity activity,List<MyAppointment.Data> appointmentList,
                                    List<Integer> lightColorList,List<Integer> darkColorList) {
        this.myAppointmentList = appointmentList;
        this.lightColorList = lightColorList;
        this.darkColorList = darkColorList;
        this.activity = activity;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMyAppointmentBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_my_appointment, parent, false);

        return new RecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        MyAppointment.Data itemData = myAppointmentList.get(position);
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;
        viewHolder.bindItem(itemData,position);

    }

    @Override
    public int getItemCount() {
        return myAppointmentList.size();
    }


    private class RecyclerViewViewHolder extends RecyclerView.ViewHolder {

        ItemMyAppointmentBinding binding;

       public RecyclerViewViewHolder(ItemMyAppointmentBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindItem(MyAppointment.Data itemData,int position) {
            binding.txtCompanyName.setText(itemData.getBookName());
            String formatMonth = ViewControll.formatDate(itemData.getBookDate(),
                    "yyyy-MM-dd", "MMMM");
            String formatDay = ViewControll.formatDate(itemData.getBookDate(),
                    "yyyy-MM-dd", "dd");
            String timeFormat = ViewControll.formatDate(itemData.getPrice(),
                    "HH:mm:ss", "hh:mm a");

            if(!formatMonth.equals(initialMonthOfList) || position==0){
                initialMonthOfList = formatMonth;
                binding.txtMonthNameSection.setText(formatMonth);
                binding.txtMonthNameSection.setVisibility(View.VISIBLE);

            }else {
                binding.txtMonthNameSection.setVisibility(View.GONE);
            }
            binding.txtDate.setText(formatDay);
            binding.txtMonth.setText(formatMonth);
            binding.txtTime.setText(timeFormat);
            binding.llDate.setBackgroundTintList(ContextCompat.getColorStateList
                    (activity, darkColorList.get(position)));
            binding.llMain.setBackgroundTintList(ContextCompat.getColorStateList
                    (activity, lightColorList.get(position)));
            binding.imgTelephone.setImageTintList(ContextCompat.getColorStateList
                    (activity, darkColorList.get(position)));
            binding.executePendingBindings();

        }
    }

}
