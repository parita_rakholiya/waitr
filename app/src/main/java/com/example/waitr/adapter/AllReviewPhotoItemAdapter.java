package com.example.waitr.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.waitr.R;
import com.example.waitr.databinding.ItemAllReviewBinding;
import com.example.waitr.databinding.ItemAllReviewPhotoItemBinding;
import com.example.waitr.model.AllReviewModel;

import java.util.ArrayList;
import java.util.List;

public class AllReviewPhotoItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<AllReviewModel> allReviewModelList = new ArrayList<>();
    private Activity activity;
    private int itemSize = 0;

    public AllReviewPhotoItemAdapter(Activity activity, List<AllReviewModel> allReviewModelList,int itemSize) {
        this.allReviewModelList = allReviewModelList;
        this.activity = activity;
        this.itemSize = itemSize;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAllReviewPhotoItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_all_review_photo_item, parent, false);

        return new RecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        AllReviewModel itemData = allReviewModelList.get(position);
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;
        viewHolder.bindItem(itemData,position);

    }

    @Override
    public int getItemCount() {
        return itemSize;
    }


    private class RecyclerViewViewHolder extends RecyclerView.ViewHolder {

        ItemAllReviewPhotoItemBinding binding;

       public RecyclerViewViewHolder(ItemAllReviewPhotoItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindItem(AllReviewModel itemData,int position) {
            binding.executePendingBindings();

        }
    }

}
