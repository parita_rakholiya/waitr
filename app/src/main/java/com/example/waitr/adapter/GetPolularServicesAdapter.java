package com.example.waitr.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.waitr.R;
import com.example.waitr.databinding.ItemGetCategoryBinding;
import com.example.waitr.databinding.ItemPopularServicesBinding;
import com.example.waitr.model.GetCategories;
import com.example.waitr.model.GetPopularServices;

import java.util.ArrayList;
import java.util.List;

public class GetPolularServicesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<GetPopularServices.Data> popularServiceList = new ArrayList<>();
    private Activity activity;

    public GetPolularServicesAdapter(Activity activity, List<GetPopularServices.Data> popularServiceList) {
        this.popularServiceList = popularServiceList;
        this.activity = activity;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemPopularServicesBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_popular_services, parent, false);

        return new RecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        GetPopularServices.Data itemData = popularServiceList.get(position);
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;
        viewHolder.bindItem(itemData,position);

    }

    @Override
    public int getItemCount() {
        return popularServiceList.size();
    }


    private class RecyclerViewViewHolder extends RecyclerView.ViewHolder {

        ItemPopularServicesBinding binding;

       public RecyclerViewViewHolder(ItemPopularServicesBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindItem(GetPopularServices.Data itemData,int position) {
            binding.txtServiceName.setText(itemData.getTitle());
//            binding.txtBookingId.setText("Booking id: "+itemData.getBookID());
//            binding.txtStatus.setText(itemData.getStatus());
//            binding.txtPrice.setText("$"+itemData.getPrice());
//            String formatDate = ViewControll.formatDate(itemData.getBookDate(),
//                    "yyyy-MM-dd", "dd MMMM yyyy");
//            String timeFormat = ViewControll.formatDate(itemData.getBookTime(),
//                    "HH:mm:ss", "hh:mm a");
//            binding.txtDate.setText(formatDate);
//            binding.txtTime.setText(timeFormat);
//            Glide.with(activity).load(APIConstant.IMAGE_BASE_URL+itemData.getBookImage())
//                    .override(500,500)
//                    .placeholder(R.drawable.app_icon)
//                    .into(binding.imgCompany);

            binding.executePendingBindings();

        }
    }

}
