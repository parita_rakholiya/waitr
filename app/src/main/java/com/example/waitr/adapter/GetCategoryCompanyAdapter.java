package com.example.waitr.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.waitr.R;
import com.example.waitr.activity.CompanyDetailsActivity;
import com.example.waitr.databinding.ItemGetCategoryCompanyBinding;
import com.example.waitr.model.GetCategories;
import com.example.waitr.utils.Constants;
import com.example.waitr.utils.ViewControll;

import java.util.ArrayList;
import java.util.List;

public class GetCategoryCompanyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<GetCategories.Data> categoryList = new ArrayList<>();
    private Activity activity;

    public GetCategoryCompanyAdapter(Activity activity, List<GetCategories.Data> categoryList) {
        this.categoryList = categoryList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemGetCategoryCompanyBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_get_category_company, parent, false);

        return new RecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        GetCategories.Data itemData = categoryList.get(position);
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;
        viewHolder.bindItem(itemData,position);

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }


    private class RecyclerViewViewHolder extends RecyclerView.ViewHolder {

        ItemGetCategoryCompanyBinding binding;

       public RecyclerViewViewHolder(ItemGetCategoryCompanyBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindItem(GetCategories.Data itemData,int position) {
            binding.txtCategory.setText(itemData.getCategory());
            binding.txtCompanyName.setText(itemData.getName());
            String formatStartDate = ViewControll.formatDate(itemData.getSatartDate(),
                    "yyyy-MM-dd", "dd/MM/yyyy");
            String formatEndDate = ViewControll.formatDate(itemData.getEndDate(),
                    "yyyy-MM-dd", "dd/MM/yyyy");
            binding.txtStartDate.setText(formatStartDate);
            binding.txtEndDate.setText(formatEndDate);
            binding.txtRate.setText(itemData.getReeting());
//            Glide.with(activity).load(APIConstant.IMAGE_BASE_URL+itemData.getImage())
//                    .override(500,500)
//                    .placeholder(R.drawable.app_icon)
//                    .into(binding.imgCompany);
            binding.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.startActivity(new Intent(activity, CompanyDetailsActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra(Constants.COMPANY_DETAILS,itemData));
                }
            });
            binding.executePendingBindings();

        }
    }

}
