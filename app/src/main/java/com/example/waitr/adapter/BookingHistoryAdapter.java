package com.example.waitr.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.waitr.R;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.databinding.ItemBookingHistoryBinding;
import com.example.waitr.databinding.ItemNotificationsBinding;
import com.example.waitr.model.BookingHistory;
import com.example.waitr.model.Notifications;
import com.example.waitr.utils.ViewControll;

import java.util.ArrayList;
import java.util.List;

public class BookingHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<BookingHistory.Data> bookingHistoryList = new ArrayList<>();
    private Activity activity;
    private String initialMonthOfList;

    public BookingHistoryAdapter(Activity activity, List<BookingHistory.Data> bookingHistoryList) {
        this.bookingHistoryList = bookingHistoryList;
        this.activity = activity;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemBookingHistoryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_booking_history, parent, false);

        return new RecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookingHistory.Data itemData = bookingHistoryList.get(position);
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;
        viewHolder.bindItem(itemData,position);

    }

    @Override
    public int getItemCount() {
        return bookingHistoryList.size();
    }


    private class RecyclerViewViewHolder extends RecyclerView.ViewHolder {

        ItemBookingHistoryBinding binding;

       public RecyclerViewViewHolder(ItemBookingHistoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindItem(BookingHistory.Data itemData,int position) {
            binding.txtTitle.setText(itemData.getBookName());
            binding.txtBookingId.setText("Booking id: "+itemData.getBookID());
            binding.txtStatus.setText(itemData.getStatus());
            binding.txtPrice.setText("$"+itemData.getPrice());
            String formatDate = ViewControll.formatDate(itemData.getBookDate(),
                    "yyyy-MM-dd", "dd MMMM yyyy");
            String timeFormat = ViewControll.formatDate(itemData.getBookTime(),
                    "HH:mm:ss", "hh:mm a");
            binding.txtDate.setText(formatDate);
            binding.txtTime.setText(timeFormat);
            Glide.with(activity).load(APIConstant.IMAGE_BASE_URL+itemData.getBookImage())
                    .override(500,500)
                    .placeholder(R.drawable.app_icon)
                    .into(binding.imgCompany);
            if(!formatDate.equals(initialMonthOfList) || position==0){
                initialMonthOfList = formatDate;
                binding.txtMonthNameSection.setText(ViewControll.setDateWiseHeader(formatDate,"dd MMMM yyyy"));
                binding.txtMonthNameSection.setVisibility(View.VISIBLE);

            }else {
                binding.txtMonthNameSection.setVisibility(View.GONE);
            }

            binding.executePendingBindings();

        }
    }

}
