package com.example.waitr.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.waitr.R;
import com.example.waitr.activity.AllReviewsActivity;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.databinding.ItemAllReviewBinding;
import com.example.waitr.databinding.ItemBookingHistoryBinding;
import com.example.waitr.model.AllReviewModel;
import com.example.waitr.model.BookingHistory;
import com.example.waitr.utils.ViewControll;

import java.util.ArrayList;
import java.util.List;

public class AllReviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<AllReviewModel> allReviewModelList = new ArrayList<>();
    private Activity activity;

    public AllReviewAdapter(Activity activity, List<AllReviewModel> allReviewModelList) {
        this.allReviewModelList = allReviewModelList;
        this.activity = activity;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAllReviewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_all_review, parent, false);

        return new RecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        AllReviewModel itemData = allReviewModelList.get(position);
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;
        viewHolder.bindItem(itemData,position);

    }

    @Override
    public int getItemCount() {
        return allReviewModelList.size();
    }


    private class RecyclerViewViewHolder extends RecyclerView.ViewHolder {

        ItemAllReviewBinding binding;

       public RecyclerViewViewHolder(ItemAllReviewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindItem(AllReviewModel itemData,int position) {
            AllReviewPhotoItemAdapter itemDataAdapter =
                    new AllReviewPhotoItemAdapter(activity,allReviewModelList,itemData.getPhotoCount());
            binding.recyclerPhotoItem.setAdapter(itemDataAdapter);
            itemDataAdapter.notifyDataSetChanged();
            binding.executePendingBindings();

        }
    }

}
