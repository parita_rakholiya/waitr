package com.example.waitr.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.waitr.R;
import com.example.waitr.activity.NotificationDetailActivity;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.databinding.ItemMyAppointmentBinding;
import com.example.waitr.databinding.ItemNotificationsBinding;
import com.example.waitr.model.MyAppointment;
import com.example.waitr.model.Notifications;
import com.example.waitr.utils.Constants;
import com.example.waitr.utils.ViewControll;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NotificationsDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Notifications.Data> notificationList = new ArrayList<>();
    private Activity activity;
    private String initialMonthOfList;

    public NotificationsDataAdapter(Activity activity, List<Notifications.Data> notificationList) {
        this.notificationList = notificationList;
        this.activity = activity;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemNotificationsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_notifications, parent, false);

        return new RecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Notifications.Data itemData = notificationList.get(position);
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;
        viewHolder.bindItem(itemData,position);

    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }


    private class RecyclerViewViewHolder extends RecyclerView.ViewHolder {

        ItemNotificationsBinding binding;

       public RecyclerViewViewHolder(ItemNotificationsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindItem(Notifications.Data itemData,int position) {
            binding.txtTitle.setText(itemData.getComment());
            binding.txtDescrip.setText(itemData.getDescription());
            String formatDate = ViewControll.formatDate(itemData.getCreatedDate(),
                    "yyyy-MM-dd HH:mm:ss", "MMMM dd");
            String checkNextDate = ViewControll.formatDate(itemData.getCreatedDate(),
                    "yyyy-MM-dd HH:mm:ss", "MMMM dd yyyy");
            binding.txtDate.setText(formatDate);
            Glide.with(activity).load(APIConstant.IMAGE_BASE_URL+itemData.getImage())
                    .override(500,500)
                    .placeholder(R.drawable.app_icon)
                    .into(binding.imgNotification);
            if(!checkNextDate.equals(initialMonthOfList) || position==0){
                initialMonthOfList = checkNextDate;
                binding.txtMonthNameSection.setText(ViewControll.setDateWiseHeader(checkNextDate,"MMMM dd yyyy"));
                binding.txtMonthNameSection.setVisibility(View.VISIBLE);

            }else {
                binding.txtMonthNameSection.setVisibility(View.GONE);
            }
            binding.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.startActivity(new Intent(activity,
                            NotificationDetailActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra(Constants.NOTIFICATIONS,itemData));
                }
            });
            binding.executePendingBindings();

        }
    }


}
