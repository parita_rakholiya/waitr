package com.example.waitr.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.waitr.R;

public class WelcomeScreenAdapter extends PagerAdapter {
    // Declare Variables
    Activity context;
    String[] title;
    String[] description;
    int[] images;


    public WelcomeScreenAdapter(Activity context, String[] title, String[] description, int[] images) {
        this.context = context;
        this.title = title;
        this.description = description;
        this.images = images;
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        // Declare Variables
        TextView txtTitle;
        TextView txtDescription;
        ImageView imgWelcome;

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.welcome_screen_item, container,
                false);

        // Locate the TextViews in viewpager_item.xml
        txtTitle = (TextView) itemView.findViewById(R.id.txt_title);
        txtDescription = (TextView) itemView.findViewById(R.id.txt_descrip);
        imgWelcome = (ImageView) itemView.findViewById(R.id.img_service);

        // Capture position and set to the TextViews
        txtTitle.setText(title[position]);
        txtDescription.setText(description[position]);
        imgWelcome.setImageResource(images[position]);

        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((LinearLayout) object);

    }
}
