package com.example.waitr.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.waitr.R;
import com.example.waitr.activity.UserProfileActivity;
import com.example.waitr.databinding.ItemProfileDataBinding;
import com.example.waitr.interfaces.ProfileItemClickListener;
import com.example.waitr.utils.Constants;
import com.example.waitr.viewmodel.ProfileScreenItemData;
import com.example.waitr.viewmodel.ProfileViewModel;

import java.util.ArrayList;
import java.util.List;

public class ProfileItemDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ProfileScreenItemData> profileScreenItemDataList;
    private ProfileItemClickListener profileItemClickListener;

    public ProfileItemDataAdapter(ProfileItemClickListener itemClickListener) {
        profileItemClickListener = itemClickListener;
        this.profileScreenItemDataList = new ArrayList<ProfileScreenItemData>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemProfileDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_profile_data, parent, false);

        return new RecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ProfileScreenItemData itemData = profileScreenItemDataList.get(position);
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;
        viewHolder.bindItem(itemData);


    }

    @Override
    public int getItemCount() {
        return profileScreenItemDataList.size();
    }

    public void updateProfileItemList(final List<ProfileScreenItemData> profileScreenItemDataList) {
        this.profileScreenItemDataList.clear();
        this.profileScreenItemDataList = profileScreenItemDataList;
        notifyDataSetChanged();
    }

    class RecyclerViewViewHolder extends RecyclerView.ViewHolder {

        ItemProfileDataBinding binding;

       public RecyclerViewViewHolder(ItemProfileDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindItem(ProfileScreenItemData itemData) {
            binding.txtTitle.setText(itemData.getItemTitle());
            binding.imgIcon.setImageResource(itemData.getImgIcon());
            binding.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(itemData.getItemTitle().equals(Constants.PERSONAL_INFO)){
                        profileItemClickListener.clickPersonalInfo();
                    }else if(itemData.getItemTitle().equals(Constants.SETTINGS)){
                        profileItemClickListener.clickSettings();
                    }else if(itemData.getItemTitle().equals(Constants.SUPPORT)){
                        profileItemClickListener.clickSupport();
                    }else if(itemData.getItemTitle().equals(Constants.PRIVACY_POLICY)){
                        profileItemClickListener.clickPrivacyPolicy();
                    }else if(itemData.getItemTitle().equals(Constants.NOTIFICATIONS)){
                        profileItemClickListener.clickNotifications();
                    }else if(itemData.getItemTitle().equals(Constants.BOOKING_HISTORY)){
                        profileItemClickListener.clickBookingHistory();
                    }else if(itemData.getItemTitle().equals(Constants.APPOINTMENT)){
                        profileItemClickListener.clickAppointments();
                    }else if(itemData.getItemTitle().equals(Constants.ABOUT_US)){
                        profileItemClickListener.clickAboutUs();
                    }else if(itemData.getItemTitle().equals(Constants.LOGOUT)){
                        profileItemClickListener.clickLogout();
                    }
                }
            });
            binding.executePendingBindings();
        }
    }
}
