package com.example.waitr.model;

import android.app.Activity;
import android.widget.Toast;

import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.loader.Loader;
import com.example.waitr.utils.PrefManager;

public class GetUserInfoApi {

    public static void getLoginUserInfo(Activity activity, String message,apiResponseListener responseListener){
        String userId = PrefManager.getString(APIConstant.KeyParameter.USER_ID,"0");
        APIManager.getLoginUserInfo(userId,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                Loader.hide(activity);
                LoginUserInfo loginUserInfo = (LoginUserInfo) modelclass;
                PrefManager.saveLoginUser(loginUserInfo);
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                responseListener.responseSuccess();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    public interface apiResponseListener{
        void responseSuccess();
    }
}
