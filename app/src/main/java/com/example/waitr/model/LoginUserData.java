package com.example.waitr.model;

public class LoginUserData {

    private String userName,userEmail,profileUrl;
    private boolean isSocialLogin =  false;

    public LoginUserData(String userName, String email, String profileUrl, boolean isSocialLogin) {
        this.userName = userName;
        this.userEmail = email;
        this.profileUrl = profileUrl;
        this.isSocialLogin = isSocialLogin;
    }

    public LoginUserData(){};

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getUserEmail() {
        return userEmail;
    }


    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }


    public boolean isSocialLogin() {
        return isSocialLogin;
    }

    public void setSocialLogin(boolean socialLogin) {
        isSocialLogin = socialLogin;
    }

}
