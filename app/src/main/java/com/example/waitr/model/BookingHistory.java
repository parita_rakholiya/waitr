package com.example.waitr.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "error",
        "status_code",
        "message"
})
public class BookingHistory implements Serializable {

    @JsonProperty("data")
    private List<Data> data = null;
    @JsonProperty("error")
    private Boolean error;
    @JsonProperty("status_code")
    private String statusCode;
    @JsonProperty("message")
    private String message;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("data")
    public List<Data> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<Data> data) {
        this.data = data;
    }

    @JsonProperty("error")
    public Boolean getError() {
        return error;
    }

    @JsonProperty("error")
    public void setError(Boolean error) {
        this.error = error;
    }

    @JsonProperty("status_code")
    public String getStatusCode() {
        return statusCode;
    }

    @JsonProperty("status_code")
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "bookID",
            "bookName",
            "price",
            "bookTime",
            "Status",
            "bookDate",
            "bookImage"
    })
    public static class Data {

        @JsonProperty("bookID")
        private String bookID;
        @JsonProperty("bookName")
        private String bookName;
        @JsonProperty("price")
        private String price;
        @JsonProperty("bookTime")
        private String bookTime;
        @JsonProperty("Status")
        private String status;
        @JsonProperty("bookDate")
        private String bookDate;
        @JsonProperty("bookImage")
        private String bookImage;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("bookID")
        public String getBookID() {
            return bookID;
        }

        @JsonProperty("bookID")
        public void setBookID(String bookID) {
            this.bookID = bookID;
        }

        @JsonProperty("bookName")
        public String getBookName() {
            return bookName;
        }

        @JsonProperty("bookName")
        public void setBookName(String bookName) {
            this.bookName = bookName;
        }

        @JsonProperty("price")
        public String getPrice() {
            return price;
        }

        @JsonProperty("price")
        public void setPrice(String price) {
            this.price = price;
        }

        @JsonProperty("bookTime")
        public String getBookTime() {
            return bookTime;
        }

        @JsonProperty("bookTime")
        public void setBookTime(String bookTime) {
            this.bookTime = bookTime;
        }

        @JsonProperty("Status")
        public String getStatus() {
            return status;
        }

        @JsonProperty("Status")
        public void setStatus(String status) {
            this.status = status;
        }

        @JsonProperty("bookDate")
        public String getBookDate() {
            return bookDate;
        }

        @JsonProperty("bookDate")
        public void setBookDate(String bookDate) {
            this.bookDate = bookDate;
        }

        @JsonProperty("bookImage")
        public String getBookImage() {
            return bookImage;
        }

        @JsonProperty("bookImage")
        public void setBookImage(String bookImage) {
            this.bookImage = bookImage;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

}

