package com.example.waitr.model;

public class AllReviewModel {

    private int photoCount;

    public AllReviewModel(int photoCount) {
        this.photoCount = photoCount;
    }

    public int getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(int photoCount) {
        this.photoCount = photoCount;
    }

}
