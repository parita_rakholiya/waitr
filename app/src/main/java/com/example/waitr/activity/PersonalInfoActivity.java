package com.example.waitr.activity;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.waitr.R;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.databinding.ActivityPersonalInfoBinding;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.GetUserInfoApi;
import com.example.waitr.model.LoginUserInfo;
import com.example.waitr.model.SendEmailPin;
import com.example.waitr.model.UserProfileData;
import com.example.waitr.utils.Constants;
import com.example.waitr.utils.FileUtils;
import com.example.waitr.utils.PrefManager;
import com.example.waitr.utils.ViewControll;
import com.jaeger.library.StatusBarUtil;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PersonalInfoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PICTURE_RESULT = 101;
    private static final int GALLERY_RESULT = 102;
    private ActivityPersonalInfoBinding personalInfoBinding;
    private Activity activity;
    private String fullName,email,phoneNumber,address,houseNo,zipCode,city;
    private Uri imageUri;
    private File pickedImagePath;
    MultipartBody.Part profileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(PersonalInfoActivity.this);
        personalInfoBinding = DataBindingUtil.setContentView(this,R.layout.activity_personal_info);
        activity = PersonalInfoActivity.this;
        initView();
    }

    private void initView() {
        ViewControll.setLoginUserProfileImage(activity,personalInfoBinding.imgProfile);
        setUserPersonalInfo();
        setCountryCodeList();
        personalInfoBinding.imgBack.setOnClickListener(this);
        personalInfoBinding.llUpdateInfo.setOnClickListener(this);
        personalInfoBinding.btnChangePhoto.setOnClickListener(this);
        personalInfoBinding.txtPhoneCode.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
        if(id==R.id.ll_update_info){
            if(checkValidation()){
                callUpdateUserInfoApi();

            }
        }
        if(id==R.id.btn_change_photo){
            showPictureDialog();
        }
        if(id==R.id.txt_phone_code){
            personalInfoBinding.txtPhoneCode.showDropDown();
        }
    }

    private void setCountryCodeList(){
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this,
                R.array.countryCodes, R.layout.list_textview);
        personalInfoBinding.txtPhoneCode.setAdapter(adapter);

    }
    private void setUserPersonalInfo(){
        LoginUserInfo loginUserInfo = PrefManager.loginUser();
        personalInfoBinding.etFullName.setText(loginUserInfo.getName());
        personalInfoBinding.etEmail.setText(loginUserInfo.getEmail());
        personalInfoBinding.etPhoneNumber.setText(loginUserInfo.getPhone());
        personalInfoBinding.etAddress.setText(loginUserInfo.getAddress());
        personalInfoBinding.etHomeNo.setText(loginUserInfo.getHousNumber());
        personalInfoBinding.etZipcode.setText(loginUserInfo.getZipCode());
        personalInfoBinding.etCity.setText(loginUserInfo.getCity());
    }

    private boolean checkValidation(){
        fullName = personalInfoBinding.etFullName.getText().toString();
        email = personalInfoBinding.etEmail.getText().toString();
        phoneNumber = personalInfoBinding.etPhoneNumber.getText().toString();
        address = personalInfoBinding.etAddress.getText().toString();
        houseNo = personalInfoBinding.etHomeNo.getText().toString();
        zipCode = personalInfoBinding.etZipcode.getText().toString();
        city = personalInfoBinding.etCity.getText().toString();

        if(fullName.isEmpty()){
            Toast.makeText(activity, "Please enter full name", Toast.LENGTH_SHORT).show();
            return false;
        }else if(TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(activity, "Please enter valid email", Toast.LENGTH_SHORT).show();
            return false;
        }else if(phoneNumber.isEmpty()){
            Toast.makeText(activity, "Please enter phone number", Toast.LENGTH_SHORT).show();
            return false;
        }else if(phoneNumber.length()<10 || !Patterns.PHONE.matcher(phoneNumber).matches()){
            Toast.makeText(activity, "Please enter valid 10 digit phone number", Toast.LENGTH_SHORT).show();
            return false;
        }else if(address.isEmpty()){
            Toast.makeText(activity, "Please enter address", Toast.LENGTH_SHORT).show();
            return false;
        }else if(houseNo.isEmpty()){
            Toast.makeText(activity, "Please enter house no", Toast.LENGTH_SHORT).show();
            return false;
        }else if(zipCode.isEmpty() || zipCode.length()<6){
            Toast.makeText(activity, "Please enter valid zipcode", Toast.LENGTH_SHORT).show();
            return false;
        }else if(city.isEmpty()){
            Toast.makeText(activity, "Please enter city", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void callUpdateUserInfoApi(){
        String userId = PrefManager.getString(APIConstant.KeyParameter.USER_ID,"0");
        HashMap<String, RequestBody> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.EMAIL,ViewControll.convertStringToRequestBody(email));
        hashMap.put(APIConstant.Parameter.NAME,ViewControll.convertStringToRequestBody(fullName));
        hashMap.put(APIConstant.Parameter.PHONE,ViewControll.convertStringToRequestBody(phoneNumber));
        hashMap.put(APIConstant.Parameter.ADDRESS,ViewControll.convertStringToRequestBody(address));
        hashMap.put(APIConstant.Parameter.HOUSE_NUMBER,ViewControll.convertStringToRequestBody(houseNo));
        hashMap.put(APIConstant.Parameter.ZIPCODE,ViewControll.convertStringToRequestBody(zipCode));
        hashMap.put(APIConstant.Parameter.CITY,ViewControll.convertStringToRequestBody(city));
        hashMap.put(APIConstant.Parameter.ID,ViewControll.convertStringToRequestBody(userId));

        if(pickedImagePath!=null){
            // Parsing any Media type file
            RequestBody requestBody = RequestBody.create(pickedImagePath,MediaType.parse("image/*"));
            profileImage = MultipartBody.Part.createFormData("profileImage",
                    pickedImagePath.getName(), requestBody);

        }

        Loader.show(activity);
        APIManager.updateUserInfoApi(hashMap,profileImage,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                Loader.hide(activity);
                SendEmailPin sendEmailPin = (SendEmailPin) modelclass;
                if(!sendEmailPin.getStatus_code().equals("200 OK")){
                    Toast.makeText(activity, sendEmailPin.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                if(!PrefManager.getBoolean(APIConstant.KeyParameter.IS_SOCIAL_LOGIN)){
                    GetUserInfoApi.getLoginUserInfo(activity, sendEmailPin.getMessage(), new GetUserInfoApi.apiResponseListener() {
                        @Override
                        public void responseSuccess() {
                            finish();
                        }
                    });
                }else {
                    finish();
                }

                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    //show dialog for choose image from camera or filemanager
    private void showPictureDialog() {
        android.app.AlertDialog.Builder pictureDialog = new android.app.AlertDialog.Builder(activity);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Open Gallery",
                "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if(ViewControll.isStoragePermissionGranted(activity))
                                openGallery();
                                break;
                            case 1:
                                if(ViewControll.isCameraPermissionGranted(activity))
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void takePhotoFromCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        activity.startActivityForResult(intent, PICTURE_RESULT);
    }

    private void openGallery(){
        if(ViewControll.isStoragePermissionGranted(activity)){
            Intent imageSelectIntent = new Intent(Intent.ACTION_PICK);
            imageSelectIntent.setType("image/*");
            activity.startActivityForResult(imageSelectIntent, GALLERY_RESULT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        // IMAGE PICKED!!
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == GALLERY_RESULT) {

                try {
                    if (data != null) {
                        Uri uri = data.getData();
                        CropImage.activity(uri)
                                .start(this);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == PICTURE_RESULT) {
                try {
                    CropImage.activity(imageUri)
                            .start(this);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    Uri resultUri = result.getUri();
                    this.pickedImagePath = new File(FileUtils.getPath(activity, resultUri));
                    Glide.with(activity).load(pickedImagePath).into(personalInfoBinding.imgProfile);
            }
        }else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Exception error = result.getError();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}