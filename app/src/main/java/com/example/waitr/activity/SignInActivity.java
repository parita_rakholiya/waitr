package com.example.waitr.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.waitr.BuildConfig;
import com.example.waitr.R;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.databinding.ActivitySignInBinding;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.GetUserInfoApi;
import com.example.waitr.model.LoginApiModel;
import com.example.waitr.model.LoginUserData;
import com.example.waitr.model.LoginUserInfo;
import com.example.waitr.model.SendEmailPin;
import com.example.waitr.utils.ClickableText;
import com.example.waitr.utils.Constants;
import com.example.waitr.utils.PrefManager;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.jaeger.library.StatusBarUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.regex.Pattern;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivitySignInBinding signInBinding;
    private Activity activity;
    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 7;
    private String TAG = "SignInActivity";
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private String isFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(SignInActivity.this);
        signInBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in);
        activity = SignInActivity.this;
        isFrom = getIntent().getExtras().getString(APIConstant.KeyParameter.IS_FROM);
        FacebookSdk.sdkInitialize(activity);
        initializeGoogleClient();
        facebookSdkInitialization();
        initView();
        printHashKey();
    }

    private void printHashKey() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    private void initView() {
        signInBinding.btnGoogleLogin.setOnClickListener(this);
        signInBinding.btnFacebookLogin.setOnClickListener(this);
        signInBinding.btnSignin.setOnClickListener(this);
        ClickableText.setClickableString(activity, "Retrive it", getString(R.string.forgot_password_txt),
                signInBinding.forgotPassword, new ClickableText.textClickListener() {
                    @Override
                    public void textClicked() {
                        startActivity(new Intent(activity,EmailAddressActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra(APIConstant.KeyParameter.IS_FROM,APIConstant.KeyParameter.FROM_FORGOT_PASSWORD));                    }
                });

        ClickableText.setClickableString(activity, "Register", getString(R.string.dont_have_account_txt),
                signInBinding.txtRegister, new ClickableText.textClickListener() {
                    @Override
                    public void textClicked() {
                        Constants.isRegisterClick = true;
                        if(isFrom.equals(APIConstant.KeyParameter.FROMSIGNIN)){
                            finish();
                        }else {
                            startActivity(new Intent(activity, AskForSignupActivity.class).
                                    setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }
                    }
                });


    }

    private void initializeGoogleClient() {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        activity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            String personName = account.getDisplayName() != null ? account.getDisplayName() : "";
            String photoUrl = account.getPhotoUrl() != null ? account.getPhotoUrl().toString() : "";
            String email = account.getEmail() != null ? account.getEmail() : "";
            saveLoginUserData(personName,email,photoUrl,true);
            signOut();
            Toast.makeText(activity, "Google login successfully", Toast.LENGTH_SHORT).show();
            gotoHomeActivity();
        } catch (ApiException e) {
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode() + "\n" + e.getLocalizedMessage() + "\nMEssg: " + e.getMessage());
        }
    }

    private void getLoginUserInfo(Activity activity,String message){
        GetUserInfoApi.getLoginUserInfo(activity, message, new GetUserInfoApi.apiResponseListener() {
            @Override
            public void responseSuccess() {
                signInBinding.etEmail.setText("");
                signInBinding.etPassword.setText("");
                gotoHomeActivity();
            }
        });
    }

    private void saveLoginUserData(String userName,String email,String photoUrl,boolean isSocialLogin){
        PrefManager.putBoolean(APIConstant.KeyParameter.IS_SOCIAL_LOGIN,isSocialLogin);
        LoginUserInfo loginUserInfo = new LoginUserInfo();
        loginUserInfo.setEmail(email);
        loginUserInfo.setName(userName);
        loginUserInfo.setProfileImage(photoUrl);
        PrefManager.saveLoginUser(loginUserInfo);
    }

    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
    }


    @Override
    public void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btn_google_login) {
            signIn();
        }
        if (id == R.id.btn_facebook_login) {
            loginButton.performClick();
        }
        if (id == R.id.btn_signin) {
            if(checkValidation()){
                callLoginApi();
            }
        }
    }

    private void facebookSdkInitialization() {
        //logout from facebook

        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

                if (currentAccessToken != null) {
                    LoginManager.getInstance().logOut();
                }

            }
        };

        loginButton = new LoginButton(activity);

        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email"));

        loginButton.setLoginBehavior(LoginBehavior.WEB_ONLY);

        // Creating CallbackManager
        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(activity, "Facebook login successfully", Toast.LENGTH_SHORT).show();
                getUserProfile(loginResult.getAccessToken());

            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

    }

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        try {
                           String first_name = "";
                           String last_name = "";
                           String email = "";
                            String id = object.getString("id");
                            if(object.has("first_name")){
                               first_name =  object.getString("first_name");
                            }
                            if(object.has("last_name")){
                                last_name =  object.getString("last_name");
                            }
                            if(object.has("email")){
                                email =  object.getString("email");
                            }
                            String userName = first_name+" "+last_name;
                            String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";
                            saveLoginUserData(userName,email,image_url,true);
                            logoutFromFacebook();
                            gotoHomeActivity();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }

    public void logoutFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // user already logged out
        }
        GraphRequest delPermRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                if(graphResponse!=null){
                    FacebookRequestError error =graphResponse.getError();
                    if(error!=null){
                        Log.e(TAG, error.toString());
                    }else {
                        AccessToken.setCurrentAccessToken(null);
                        Profile.setCurrentProfile(null);
                        LoginManager.getInstance().logOut();
                    }
                }
            }
        });
        Log.d(TAG,"Executing revoke permissions with graph path" + delPermRequest.getGraphPath());
        delPermRequest.executeAsync();
    }

    private void callLoginApi(){
        String email = signInBinding.etEmail.getText().toString();
        String password = signInBinding.etPassword.getText().toString();
        Loader.show(activity);
        APIManager.loginApi(email,password,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                LoginApiModel loginApiModel = (LoginApiModel) modelclass;
                if(!loginApiModel.getStatusCode().equals("200 OK")){
                    Loader.hide(activity);
                    Toast.makeText(activity, loginApiModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                PrefManager.putString(APIConstant.KeyParameter.USER_ID,loginApiModel.getUser().getId());
                getLoginUserInfo(activity,loginApiModel.getMessage());
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private void gotoHomeActivity(){
        startActivity(new Intent(activity, HomeScreenActivity.class).
                setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        |Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();
    }

    private boolean checkValidation(){
        String email = signInBinding.etEmail.getText().toString();
        String password = signInBinding.etPassword.getText().toString();
        if(TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(activity, "Please enter valid email", Toast.LENGTH_SHORT).show();
            return false;
        }else if(password.isEmpty()){
            Toast.makeText(activity, "Please enter password", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

}
