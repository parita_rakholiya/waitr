package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.databinding.ActivityAddPhoneNumberBinding;
import com.example.waitr.model.UserProfileData;
import com.example.waitr.utils.Constants;
import com.jaeger.library.StatusBarUtil;

public class AddPhoneNumberActivity extends AppCompatActivity implements View.OnClickListener{

    private ActivityAddPhoneNumberBinding phoneNumberBinding;
    private Activity activity;
    private String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(AddPhoneNumberActivity.this);
        phoneNumberBinding = DataBindingUtil.setContentView(this,R.layout.activity_add_phone_number);
        activity  = AddPhoneNumberActivity.this;
        initView();
    }

    private void initView() {
        phoneNumberBinding.txtPhoneCode.setOnClickListener(this);
        phoneNumberBinding.llContinue.setOnClickListener(this);
        phoneNumberBinding.imgBack.setOnClickListener(this);
        setCountryCodeList();
    }

    private void setCountryCodeList(){
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this,
                R.array.countryCodes, R.layout.list_textview);
        phoneNumberBinding.txtPhoneCode.setAdapter(adapter);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.txt_phone_code){
            phoneNumberBinding.txtPhoneCode.showDropDown();
        }
        if(id==R.id.ll_continue){
            if(checkValidation()){
                UserProfileData profileData = Constants.loginUserProfileData;
                profileData.setPhoneNumber(phoneNumber);
                Constants.loginUserProfileData = profileData;
                startActivity(new Intent(activity,
                        AddLocationActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        }
        if(id==R.id.img_back){
            onBackPressed();
        }
    }

    private boolean checkValidation(){
         phoneNumber = phoneNumberBinding.etPhoneNumber.getText().toString();
        if(phoneNumber.isEmpty()){
            Toast.makeText(activity, "Please enter phone number", Toast.LENGTH_SHORT).show();
            return false;
        }else if(phoneNumber.length()<10 || !Patterns.PHONE.matcher(phoneNumber).matches()){
            Toast.makeText(activity, "Please enter valid 10 digit phone number", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
}