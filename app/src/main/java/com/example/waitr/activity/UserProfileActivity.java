package com.example.waitr.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.Glide;
import com.example.waitr.R;
import com.example.waitr.adapter.ProfileItemDataAdapter;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.databinding.ActivityUserProfileBinding;
import com.example.waitr.interfaces.ProfileItemClickListener;
import com.example.waitr.interfaces.dialogPositiveClickListener;
import com.example.waitr.model.LoginUserData;
import com.example.waitr.model.LoginUserInfo;
import com.example.waitr.utils.PrefManager;
import com.example.waitr.utils.ViewControll;
import com.example.waitr.viewmodel.ProfileScreenItemData;
import com.example.waitr.viewmodel.ProfileViewModel;
import com.jaeger.library.StatusBarUtil;

import java.util.List;

public class UserProfileActivity extends AppCompatActivity implements View.OnClickListener, ProfileItemClickListener {

    private ActivityUserProfileBinding userProfileBinding;
    private Activity activity;
    private ProfileViewModel profileViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(UserProfileActivity.this);
        userProfileBinding = DataBindingUtil.setContentView(this,R.layout.activity_user_profile);
        activity = UserProfileActivity.this;
        profileViewModel =
                new ViewModelProvider(this).get(ProfileViewModel.class);
        initView();
    }

    private void initView(){
        userProfileBinding.imgBack.setOnClickListener(this);
        setProfileScreenItemList();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLoginUserProfileinfo();
    }

    private void getLoginUserProfileinfo(){
        ViewControll.setLoginUserProfileImage(activity,userProfileBinding.imgProfile);
       if(PrefManager.isUserExist()){
           LoginUserInfo userData = PrefManager.loginUser();
           userProfileBinding.txtUsername.setText(userData.getName());
           userProfileBinding.txtEmail.setText(userData.getEmail());
       }
    }

    private void setProfileScreenItemList(){
        ProfileItemDataAdapter itemDataAdapter = new ProfileItemDataAdapter(this);
        userProfileBinding.recyclerProfileItem.setAdapter(itemDataAdapter);
        profileViewModel.getProfileItemData().observe(this, new Observer<List<ProfileScreenItemData>>() {
            @Override
            public void onChanged(@Nullable List<ProfileScreenItemData> profileScreenItemData) {
                itemDataAdapter.updateProfileItemList(profileScreenItemData);
            }
        });
    }

    private void logout(){
        ViewControll.showAlertDialog(activity, getString(R.string.logout),
                getString(R.string.logout_msg), new dialogPositiveClickListener() {
                    @Override
                    public void positiveClick() {
                        PrefManager.deleteuser();
                        startActivity(new Intent(activity,SignInActivity.class).
                                setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra(APIConstant.KeyParameter.IS_FROM,
                                        APIConstant.KeyParameter.BACKFROMLOGOUT));
                        finishAffinity();
                    }
                });

    }

    @Override
    public void clickPersonalInfo() {
        startActivity(new Intent(activity,PersonalInfoActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void clickSettings() {
        startActivity(new Intent(activity,SettingActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void clickSupport() {
        startActivity(new Intent(activity,SupportActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void clickPrivacyPolicy() {
        startActivity(new Intent(activity,PrivacyPolicyActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void clickNotifications() {
        startActivity(new Intent(activity,NotificationsActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void clickBookingHistory() {
        startActivity(new Intent(activity,BookingHistoryActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void clickAppointments() {
        startActivity(new Intent(activity,MyAppointmentActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void clickAboutUs() {
        startActivity(new Intent(activity,AboutUsActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void clickLogout() {
        logout();
    }
}