package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.example.waitr.R;
import com.example.waitr.databinding.ActivityPersonalInfoBinding;
import com.example.waitr.databinding.ActivitySettingBinding;
import com.jaeger.library.StatusBarUtil;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivitySettingBinding settingBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(SettingActivity.this);
        settingBinding = DataBindingUtil.setContentView(this,R.layout.activity_setting);
        activity = SettingActivity.this;
        initView();
    }

    private void initView() {
        settingBinding.imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
    }
}