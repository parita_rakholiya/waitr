package com.example.waitr.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import com.example.waitr.R;
import com.example.waitr.utils.PrefManager;
import com.jaeger.library.StatusBarUtil;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(SplashScreenActivity.this);
        setContentView(R.layout.activity_splash_screen);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if(PrefManager.isUserExist()){
                    startActivity(new Intent(getApplicationContext(),HomeScreenActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    startActivity(new Intent(getApplicationContext(),WelcomeScreenActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }

            }
        },3000);

    }

}