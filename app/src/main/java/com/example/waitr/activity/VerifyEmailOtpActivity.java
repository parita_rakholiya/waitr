package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.databinding.ActivityVerificationEmailBinding;
import com.example.waitr.interfaces.dialogPositiveClickListener;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.SendEmailPin;
import com.example.waitr.utils.ClickableText;
import com.example.waitr.utils.Constants;
import com.example.waitr.utils.ViewControll;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.jaeger.library.StatusBarUtil;

public class VerifyEmailOtpActivity extends AppCompatActivity implements View.OnClickListener{

    private ActivityVerificationEmailBinding verificationEmailBinding;
    private Activity activity;
    private CountDownTimer countDownTimer;
    private String isFrom;
    private String otpText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(VerifyEmailOtpActivity.this);
        verificationEmailBinding = DataBindingUtil.setContentView(this,R.layout.activity_verification_email);
        activity = VerifyEmailOtpActivity.this;
        initView();
        reverseTimer(60,verificationEmailBinding.tvSecond);
    }

    private void initView(){
        isFrom = getIntent().getExtras().getString(APIConstant.KeyParameter.IS_FROM);
        if (isFrom.equals(APIConstant.KeyParameter.FROM_FORGOT_PASSWORD)) {
            verificationEmailBinding.txtToolbarTitle.setText(getString(R.string.forget_password));
        } else {
            verificationEmailBinding.txtToolbarTitle.setText(getString(R.string.email_address));
        }
        ClickableText.setClickableString(activity, "Resend", getString(R.string.dont_receive_code_txt),
                verificationEmailBinding.txtResend, new ClickableText.textClickListener() {
                    @Override
                    public void textClicked() {
                        reSendEmailOTPApi();
                    }
                });
        verificationEmailBinding.imgBack.setOnClickListener(this);
        verificationEmailBinding.btnVerifyOtp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
        if(id==R.id.btn_verify_otp){
            if(checkForOtpValidation()){
                setEmailOTPApi(otpText);
            }else {
                Toast.makeText(activity, "Please enter valid otp", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private boolean checkForOtpValidation(){
        otpText = verificationEmailBinding.otpView.getOTP();
        if(otpText==null || otpText.isEmpty()){
            return false;
        }else if(otpText.length()==5){
            return true;
        }
        return false;
    }

    private void reSendEmailOTPApi(){
        String email = Constants.registeredEmail;
        Loader.show(activity);
        APIManager.sendEmailOtpApi(email,"",new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                Loader.hide(activity);
                SendEmailPin sendEmailPin = (SendEmailPin) modelclass;
                if(!sendEmailPin.getStatus_code().equals("200 OK")){
                    Toast.makeText(activity, sendEmailPin.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                Toast.makeText(activity, sendEmailPin.getMessage(), Toast.LENGTH_SHORT).show();
                reverseTimer(60,verificationEmailBinding.tvSecond);
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private void setEmailOTPApi(String otp){
        String email = Constants.registeredEmail;
        Loader.show(activity);
        APIManager.setEmailOtpApi(otp,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                Loader.hide(activity);
                SendEmailPin sendEmailPin = (SendEmailPin) modelclass;
                if(!sendEmailPin.getStatus_code().equals("200 OK")){
                    Toast.makeText(activity, sendEmailPin.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                Toast.makeText(activity, sendEmailPin.getMessage(), Toast.LENGTH_SHORT).show();
                showBottomSheetDialog();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    public void reverseTimer(int Seconds,final TextView tv){

       countDownTimer = new CountDownTimer(Seconds* 1000+1000, 1000) {

            public void onTick(long millisUntilFinished) {
                verificationEmailBinding.llTimer.setVisibility(View.VISIBLE);
                verificationEmailBinding.txtResend.setEnabled(false);
                int seconds = (int) (millisUntilFinished / 1000);
                seconds = seconds % 60;
                tv.setText(String.format("%02d", seconds) +"s");
            }

            public void onFinish() {
                verificationEmailBinding.llTimer.setVisibility(View.GONE);
                verificationEmailBinding.txtResend.setEnabled(true);
            }
        }.start();
    }

    private void showBottomSheetDialog() {

        String dialogMsg = "";
        String dialogBtnTxt = "";
        if(isFrom.equals(APIConstant.KeyParameter.FROM_REGISTER_USER)){
            dialogMsg = getString(R.string.otp_has_been_verified);
            dialogBtnTxt = getString(R.string.continue_txt);
        }else {
            dialogMsg = getString(R.string.otp_has_been_verified);
            dialogBtnTxt = getString(R.string.reset_password);
        }

        ViewControll.showBottomSheetDialog(activity,dialogMsg,
               dialogBtnTxt,new dialogPositiveClickListener() {
            @Override
            public void positiveClick() {
                if(isFrom.equals(APIConstant.KeyParameter.FROM_REGISTER_USER)) {
                    startActivity(new Intent(activity, CreateFullNameActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    startActivity(new Intent(activity, SetPasswordActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .putExtra(APIConstant.KeyParameter.IS_FROM,APIConstant.KeyParameter.FROM_RESET_PASSWORD));
                }

                CheckMailBoxActivity.getInstance().finish();
                finish();
            }
        });
    }
}