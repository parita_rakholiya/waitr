package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.databinding.ActivityPrivacyPolicyBinding;
import com.jaeger.library.StatusBarUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PrivacyPolicyActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityPrivacyPolicyBinding privacyPolicyBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(PrivacyPolicyActivity.this);
        privacyPolicyBinding = DataBindingUtil.setContentView(this,R.layout.activity_privacy_policy);
        activity =  PrivacyPolicyActivity.this;
        initView();
    }

    private void initView() {
       privacyPolicyBinding.imgBack.setOnClickListener(this);
       setPrivacyPolicyTextFromFile();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
    }

    private void setPrivacyPolicyTextFromFile() {
        BufferedReader reader = null;
        StringBuilder text = new StringBuilder();

        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("privacy_policy")));

            String line;
            while ((line = reader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            Toast.makeText(activity, "Error reading file!"+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                privacyPolicyBinding.txtPrivacyPolicy.setText(Html.fromHtml(String.valueOf((CharSequence) text),Html.FROM_HTML_MODE_LEGACY));
            }else {
                privacyPolicyBinding.txtPrivacyPolicy.setText(Html.fromHtml(String.valueOf((CharSequence) text)));
            }
        }
    }
}