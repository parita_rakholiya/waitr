package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import com.example.waitr.R;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.databinding.ActivityAskForSignupBinding;
import com.example.waitr.utils.Constants;
import com.jaeger.library.StatusBarUtil;

public class AskForSignupActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityAskForSignupBinding askForSignupBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(AskForSignupActivity.this);
        askForSignupBinding = DataBindingUtil.setContentView(this,R.layout.activity_ask_for_signup);
        activity = AskForSignupActivity.this;
        initView();
    }

    private void initView() {
        askForSignupBinding.btnSignin.setOnClickListener(this);
        askForSignupBinding.btnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.btn_register){
            if(askForSignupBinding.btnRegister.getText().toString().equals(getString(R.string.register))){
                askForSignupBinding.llText.setVisibility(View.GONE);
                askForSignupBinding.btnRegister.setText(getString(R.string.register_as_company));
                askForSignupBinding.btnSignin.setText(getString(R.string.register_as_user));
            }

        }else if(id==R.id.btn_signin){
            if(askForSignupBinding.btnSignin.getText().toString().equals(getString(R.string.sign_in))){
                startActivity(new Intent(activity,SignInActivity.class).
                        setFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra(APIConstant.KeyParameter.IS_FROM,
                        APIConstant.KeyParameter.FROMSIGNIN));
            }else {
                startActivity(new Intent(activity,EmailAddressActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(APIConstant.KeyParameter.IS_FROM,APIConstant.KeyParameter.FROM_REGISTER_USER));
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(askForSignupBinding.btnRegister.getText().toString().equals(getString(R.string.register))){
           super.onBackPressed();
        }else {
            askForSignupBinding.llText.setVisibility(View.VISIBLE);
            askForSignupBinding.btnRegister.setText(getString(R.string.register));
            askForSignupBinding.btnSignin.setText(getString(R.string.sign_in));
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Constants.isRegisterClick){
            askForSignupBinding.llText.setVisibility(View.GONE);
            askForSignupBinding.btnRegister.setText(getString(R.string.register_as_company));
            askForSignupBinding.btnSignin.setText(getString(R.string.register_as_user));
            Constants.isRegisterClick = false;
        }
    }
}