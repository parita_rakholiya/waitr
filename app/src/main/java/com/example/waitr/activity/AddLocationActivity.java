package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.databinding.ActivityAddLocationBinding;
import com.example.waitr.model.UserProfileData;
import com.example.waitr.utils.Constants;
import com.jaeger.library.StatusBarUtil;

public class AddLocationActivity extends AppCompatActivity implements View.OnClickListener{

    private ActivityAddLocationBinding locationBinding;
    private Activity activity;
    private String address,homeNo,zipCode,city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(AddLocationActivity.this);
        locationBinding = DataBindingUtil.setContentView(this,
                R.layout.activity_add_location);
        activity = AddLocationActivity.this;
        initView();
    }

    private void initView() {
        locationBinding.llSave.setOnClickListener(this);
        locationBinding.imgBack.setOnClickListener(this);
    }

    private boolean checkValidation(){
        address = locationBinding.etAddress.getText().toString();
        homeNo = locationBinding.etHomeNo.getText().toString();
        zipCode = locationBinding.etZipcode.getText().toString();
        city = locationBinding.etCity.getText().toString();
        if(address.isEmpty()){
            Toast.makeText(activity, "Please enter address", Toast.LENGTH_SHORT).show();
            return false;
        }else if(homeNo.isEmpty()){
            Toast.makeText(activity, "Please enter home no", Toast.LENGTH_SHORT).show();
            return false;
        }else if(zipCode.isEmpty() || zipCode.length()<6){
            Toast.makeText(activity, "Please enter valid zipcode", Toast.LENGTH_SHORT).show();
            return false;
        }else if(city.isEmpty()){
            Toast.makeText(activity, "Please enter city", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.ll_save){
            if(checkValidation()){
                UserProfileData profileData =  Constants.loginUserProfileData;
                profileData.setAddress(address);
                profileData.setHomeNo(homeNo);
                profileData.setZipcode(zipCode);
                profileData.setCity(city);
                Constants.loginUserProfileData = profileData;
                startActivity(new Intent(activity, SetPasswordActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(APIConstant.KeyParameter.IS_FROM,APIConstant.KeyParameter.FROM_CREATE_NEW));
            }
        }
        if(id==R.id.img_back){
            onBackPressed();
        }
    }
}