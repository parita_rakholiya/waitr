package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.waitr.R;
import com.example.waitr.adapter.AllReviewAdapter;
import com.example.waitr.adapter.BookingHistoryAdapter;
import com.example.waitr.databinding.ActivityAllReviewsBinding;
import com.example.waitr.model.AllReviewModel;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

public class AllReviewsActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityAllReviewsBinding allReviewsBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(AllReviewsActivity.this);
        allReviewsBinding = DataBindingUtil.setContentView(this,R.layout.activity_all_reviews);
        activity = AllReviewsActivity.this;
        initView();
    }

    private void initView() {
        allReviewsBinding.imgBack.setOnClickListener(this);
        AllReviewAdapter itemDataAdapter =
                new AllReviewAdapter(activity,setAllReviewsList());
        allReviewsBinding.recyclerAllReviews.setAdapter(itemDataAdapter);
        itemDataAdapter.notifyDataSetChanged();
    }

    private List<AllReviewModel> setAllReviewsList(){
        List<AllReviewModel> allReviewModelList = new ArrayList<>();
        allReviewModelList.add(new AllReviewModel(0));
        allReviewModelList.add(new AllReviewModel(1));
        allReviewModelList.add(new AllReviewModel(0));
        allReviewModelList.add(new AllReviewModel(2));
        allReviewModelList.add(new AllReviewModel(0));
        allReviewModelList.add(new AllReviewModel(3));
        allReviewModelList.add(new AllReviewModel(0));
        allReviewModelList.add(new AllReviewModel(4));
        allReviewModelList.add(new AllReviewModel(1));
        return allReviewModelList;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
    }
}