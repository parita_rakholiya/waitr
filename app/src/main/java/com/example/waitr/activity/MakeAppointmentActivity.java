package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.databinding.ActivityMakeAppointmentBinding;
import com.example.waitr.interfaces.dialogPositiveClickListener;
import com.example.waitr.utils.ViewControll;
import com.jaeger.library.StatusBarUtil;

import java.util.Calendar;
import java.util.Date;

public class MakeAppointmentActivity extends AppCompatActivity implements View.OnClickListener{

    private ActivityMakeAppointmentBinding makeAppointmentBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(MakeAppointmentActivity.this);
        makeAppointmentBinding = DataBindingUtil.setContentView(this,R.layout.activity_make_appointment);
        activity = MakeAppointmentActivity.this;
        initView();
    }

    private void initView() {
        makeAppointmentBinding.imgBack.setOnClickListener(this);
        makeAppointmentBinding.btnBook.setOnClickListener(this);
        setMinDateCalender();
    }

    private void setMinDateCalender(){
        makeAppointmentBinding.calenderView.setMinDate((new Date().getTime()));
        makeAppointmentBinding.calenderView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView calendarView, int year, int month, int day) {
                Toast.makeText(activity, "Selected date:= "+day + "-" + (month+1) + "-" + year, Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void showBottomSheetDialog() {
        String dialogMsg = getString(R.string.request_sent_successfully);
        String dialogBtnTxt = getString(R.string.done);

        ViewControll.showBottomSheetDialog(activity,dialogMsg,
                dialogBtnTxt,new dialogPositiveClickListener() {
                    @Override
                    public void positiveClick() {

                        finish();
                    }
                });

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
        if(id==R.id.btn_book){
            showBottomSheetDialog();
        }
    }
}