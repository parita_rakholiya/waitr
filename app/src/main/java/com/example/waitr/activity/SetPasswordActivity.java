package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.databinding.ActivitySetPasswordBinding;
import com.example.waitr.interfaces.dialogPositiveClickListener;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.SendEmailPin;
import com.example.waitr.model.UserProfileData;
import com.example.waitr.utils.Constants;
import com.example.waitr.utils.ViewControll;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.jaeger.library.StatusBarUtil;

import java.util.HashMap;

public class SetPasswordActivity extends AppCompatActivity implements View.OnClickListener{

    private ActivitySetPasswordBinding setPasswordBinding;
    private Activity activity;
    private String newPassword,reEnterPassword;
    private String isFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(SetPasswordActivity.this);
        setPasswordBinding  = DataBindingUtil.setContentView(this,R.layout.activity_set_password);
        activity = SetPasswordActivity.this;
        initView();
    }

    private void initView() {
        isFrom = getIntent().getExtras().getString(APIConstant.KeyParameter.IS_FROM);
        if (isFrom.equals(APIConstant.KeyParameter.FROM_RESET_PASSWORD)) {
            setPasswordBinding.txtToolbarTitle.setText(getString(R.string.forget_password));
            setPasswordBinding.txtTitle.setText(getString(R.string.create_new));
            setPasswordBinding.txtDescrip.setText(getString(R.string.create_new_password));
            setPasswordBinding.btnSubmit.setText(getString(R.string.chabge_password));
        } else {
            setPasswordBinding.txtToolbarTitle.setText(getString(R.string.set_password));
            setPasswordBinding.txtTitle.setText(getString(R.string.set_password));
            setPasswordBinding.txtDescrip.setText(getString(R.string.create_password));
            setPasswordBinding.btnSubmit.setText(getString(R.string.submit));
        }
        setPasswordBinding.imgBack.setOnClickListener(this);
        setPasswordBinding.btnSubmit.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
        if(id==R.id.btn_submit){
            if(checkValidation()){
                if (isFrom.equals(APIConstant.KeyParameter.FROM_RESET_PASSWORD)) {
                    showBottomDailog();
                }else {
                    UserProfileData profileData =  Constants.loginUserProfileData;
                    profileData.setPassword(newPassword);
                    Constants.loginUserProfileData = profileData;
                    callSignUpApi();
                }

            }
        }
    }

    private boolean checkValidation(){
        newPassword = setPasswordBinding.etNewPassword.getText().toString();
        reEnterPassword = setPasswordBinding.etReEnterPassword.getText().toString();
        if(newPassword.isEmpty()){
            Toast.makeText(activity, "Please set strong password", Toast.LENGTH_SHORT).show();
            return false;
        }else if(reEnterPassword.isEmpty()){
            Toast.makeText(activity, "Please re-enter password", Toast.LENGTH_SHORT).show();
            return false;
        }else if(!newPassword.equals(reEnterPassword)){
            Toast.makeText(activity, "Re-enter password does not match.", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void callSignUpApi(){
        String email = Constants.registeredEmail;
        UserProfileData profileData = Constants.loginUserProfileData;
        String userName = profileData.getFirstName() + " " + profileData.getLastName();
        String password = profileData.getPassword();
        String phoneNumber = profileData.getPhoneNumber();
        String address = profileData.getAddress();
        String houseNo = profileData.getHomeNo();
        String zipCode = profileData.getZipcode();
        String city = profileData.getCity();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.EMAIL,email);
        hashMap.put(APIConstant.Parameter.PASSWORD,password);
        hashMap.put(APIConstant.Parameter.NAME,userName);
        hashMap.put(APIConstant.Parameter.PHONE,phoneNumber);
        hashMap.put(APIConstant.Parameter.ADDRESS,address);
        hashMap.put(APIConstant.Parameter.HOUSE_NUMBER,houseNo);
        hashMap.put(APIConstant.Parameter.ZIPCODE,zipCode);
        hashMap.put(APIConstant.Parameter.CITY,city);
        hashMap.put(APIConstant.Parameter.TYPE,"Customers");

        Loader.show(activity);
        APIManager.signUpApi(hashMap,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                Loader.hide(activity);
                SendEmailPin sendEmailPin = (SendEmailPin) modelclass;
                if(!sendEmailPin.getStatus_code().equals("200 OK")){
                    Toast.makeText(activity, sendEmailPin.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                Toast.makeText(activity, sendEmailPin.getMessage(), Toast.LENGTH_SHORT).show();
                showBottomDailog();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private void showBottomDailog(){
        String dialogMsg = "" ;
        if(isFrom.equals(APIConstant.KeyParameter.FROM_CREATE_NEW)){
            dialogMsg =  getString(R.string.account_has_been_created);
        }else {
            dialogMsg =  getString(R.string.new_pass_has_been_created);
        }
        ViewControll.showBottomSheetDialog(activity,dialogMsg,
                getString(R.string.back_to_login),new dialogPositiveClickListener() {
                    @Override
                    public void positiveClick() {
                        startActivity(new Intent(activity,SignInActivity.class).
                                setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra(APIConstant.KeyParameter.IS_FROM,
                                        APIConstant.KeyParameter.BACKTOLOGIN));
                        finishAffinity();
                    }
                });
    }

}