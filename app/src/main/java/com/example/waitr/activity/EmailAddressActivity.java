package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.databinding.ActivityEmailAddressBinding;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.SendEmailPin;
import com.example.waitr.utils.Constants;
import com.jaeger.library.StatusBarUtil;

public class EmailAddressActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityEmailAddressBinding emailAddressBinding;
    private Activity activity;
    private String isFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(EmailAddressActivity.this);
        emailAddressBinding = DataBindingUtil.setContentView(this, R.layout.activity_email_address);
        activity = EmailAddressActivity.this;
        initView();
    }

    private void initView() {
        isFrom = getIntent().getExtras().getString(APIConstant.KeyParameter.IS_FROM);
        if (isFrom.equals(APIConstant.KeyParameter.FROM_FORGOT_PASSWORD)) {
            emailAddressBinding.txtToolbarTitle.setText(getString(R.string.forget_password));
        } else {
            emailAddressBinding.txtToolbarTitle.setText(getString(R.string.email_address));
        }
        emailAddressBinding.imgBack.setOnClickListener(this);
        emailAddressBinding.llContinue.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.img_back) {
            onBackPressed();
        }
        if (id == R.id.ll_continue) {
            if(isValidEmail(emailAddressBinding.etEmail.getText().toString())){
                if (isFrom.equals(APIConstant.KeyParameter.FROM_FORGOT_PASSWORD)) {
                    callForgotPasswordApi();
                }else {
                    callSendEmailOTPApi();
                }
            }else {
                Toast.makeText(activity, "Please enter valid email", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private  boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void callSendEmailOTPApi(){
        String email = emailAddressBinding.etEmail.getText().toString();
        Loader.show(activity);
        APIManager.sendEmailOtpApi(email,"",new APICaller.APICallBack(){
            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                Loader.hide(activity);
                SendEmailPin sendEmailPin = (SendEmailPin) modelclass;
                if(!sendEmailPin.getStatus_code().equals("200 OK")){
                    Toast.makeText(activity, sendEmailPin.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                Constants.registeredEmail = email;
                Toast.makeText(activity, sendEmailPin.getMessage(), Toast.LENGTH_SHORT).show();
                emailAddressBinding.etEmail.setText("");
                startActivity(new Intent(activity, CheckMailBoxActivity.class).
                        setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra(APIConstant.KeyParameter.IS_FROM,isFrom));
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private void callForgotPasswordApi(){
        String email = emailAddressBinding.etEmail.getText().toString();
        Loader.show(activity);
        APIManager.forgotPasswordApi(email,"",new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                Loader.hide(activity);
                SendEmailPin sendEmailPin = (SendEmailPin) modelclass;
                if(!sendEmailPin.getStatus_code().equals("200 OK")){
                    Toast.makeText(activity, sendEmailPin.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                Toast.makeText(activity, sendEmailPin.getMessage(), Toast.LENGTH_SHORT).show();
                emailAddressBinding.etEmail.setText("");
                startActivity(new Intent(activity, CheckMailBoxActivity.class).
                        setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra(APIConstant.KeyParameter.IS_FROM,isFrom));
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }
}
