package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.waitr.R;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.databinding.ActivityCheckMailBoxBinding;
import com.example.waitr.utils.Constants;
import com.jaeger.library.StatusBarUtil;

public class CheckMailBoxActivity extends AppCompatActivity implements View.OnClickListener{

    private ActivityCheckMailBoxBinding checkMailBoxBinding;
    private static Activity activity;
    private String isFrom;

    public static CheckMailBoxActivity getInstance(){
        return (CheckMailBoxActivity) activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(CheckMailBoxActivity.this);
        checkMailBoxBinding = DataBindingUtil.setContentView(this,R.layout.activity_check_mail_box);
        activity = CheckMailBoxActivity.this;
        initView();
    }

    private void initView() {
        isFrom = getIntent().getExtras().getString(APIConstant.KeyParameter.IS_FROM);
        if (isFrom.equals(APIConstant.KeyParameter.FROM_FORGOT_PASSWORD)) {
            checkMailBoxBinding.txtToolbarTitle.setText(getString(R.string.forget_password));
        } else {
            checkMailBoxBinding.txtToolbarTitle.setText(getString(R.string.email_address));
        }
        checkMailBoxBinding.btnGetCodeManual.setOnClickListener(this);
        checkMailBoxBinding.btnOpenMailbox.setOnClickListener(this);
        checkMailBoxBinding.imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.btn_get_code_manual){
            startActivity(new Intent(activity, VerifyEmailOtpActivity.class)
            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra(APIConstant.KeyParameter.IS_FROM,isFrom));
        }
        if(id==R.id.btn_open_mailbox){
            openGmailApp();
        }
        if(id==R.id.img_back){
            onBackPressed();
        }

    }

    private void openGmailApp(){
        try {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(Intent.CATEGORY_APP_EMAIL);
            activity.startActivity(intent);
        } catch (android.content.ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }
}