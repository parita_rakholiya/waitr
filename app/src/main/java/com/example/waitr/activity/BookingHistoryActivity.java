package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.adapter.BookingHistoryAdapter;
import com.example.waitr.adapter.NotificationsDataAdapter;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.databinding.ActivityBookingHistoryBinding;
import com.example.waitr.databinding.ActivityNotificationsBinding;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.BookingHistory;
import com.example.waitr.model.Notifications;
import com.jaeger.library.StatusBarUtil;

public class BookingHistoryActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityBookingHistoryBinding bookingHistoryBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(BookingHistoryActivity.this);
        bookingHistoryBinding = DataBindingUtil.setContentView(this,R.layout.activity_booking_history);
        activity =  BookingHistoryActivity.this;
        initView();
    }

    private void initView() {
        bookingHistoryBinding.imgBack.setOnClickListener(this);
        getBookingHistoryList();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
    }

    private void getBookingHistoryList(){
        Loader.show(activity);
        APIManager.getBookingHistoryApi(new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                BookingHistory bookingHistory = (BookingHistory) modelclass;
                Loader.hide(activity);
                if(!bookingHistory.getStatusCode().equals("200 OK")){
                    Toast.makeText(activity, bookingHistory.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                BookingHistoryAdapter itemDataAdapter =
                        new BookingHistoryAdapter(activity,bookingHistory.getData());
                bookingHistoryBinding.recyclerBookingHistory.setAdapter(itemDataAdapter);
                itemDataAdapter.notifyDataSetChanged();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

}