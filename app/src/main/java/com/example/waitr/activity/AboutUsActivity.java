package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Toast;

import com.example.waitr.BuildConfig;
import com.example.waitr.R;
import com.example.waitr.databinding.ActivityAboutUsBinding;
import com.example.waitr.databinding.ActivityPrivacyPolicyBinding;
import com.jaeger.library.StatusBarUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AboutUsActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityAboutUsBinding aboutUsBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(AboutUsActivity.this);
        aboutUsBinding = DataBindingUtil.setContentView(this,R.layout.activity_about_us);
        activity =  AboutUsActivity.this;
        initView();
    }

    private void initView() {
        aboutUsBinding.imgBack.setOnClickListener(this);
        aboutUsBinding.txtVersion.setText(BuildConfig.VERSION_NAME);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
    }

}