package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.databinding.ActivityCreateFullNameBinding;
import com.example.waitr.model.UserProfileData;
import com.example.waitr.utils.Constants;
import com.jaeger.library.StatusBarUtil;

public class CreateFullNameActivity extends AppCompatActivity implements View.OnClickListener{

    private ActivityCreateFullNameBinding fullNameBinding;
    private Activity activity;
    private String firstName,lastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(CreateFullNameActivity.this);
        fullNameBinding  = DataBindingUtil.setContentView(this,R.layout.activity_create_full_name);
        activity = CreateFullNameActivity.this;
        initView();
    }

    private void initView() {
        fullNameBinding.llContinue.setOnClickListener(this);
        fullNameBinding.imgBack.setOnClickListener(this);
    }

    private boolean checkValidation(){
         firstName = fullNameBinding.etFirstName.getText().toString();
         lastName = fullNameBinding.etLastName.getText().toString();
        if(firstName.isEmpty()){
            Toast.makeText(activity, "Please enter first name", Toast.LENGTH_SHORT).show();
            return false;
        }else if(lastName.isEmpty()){
            Toast.makeText(activity, "Please enter last name", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.ll_continue){
            if(checkValidation()){
                UserProfileData profileData =  new UserProfileData();
                profileData.setFirstName(firstName);
                profileData.setLastName(lastName);
                Constants.loginUserProfileData = profileData;
                startActivity(new Intent(activity,AddPhoneNumberActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

            }
        }
        if(id==R.id.img_back){
            onBackPressed();
        }
    }
}