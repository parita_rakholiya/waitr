package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.adapter.MyAppointmentDataAdapter;
import com.example.waitr.adapter.ProfileItemDataAdapter;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.databinding.ActivityMyAppointmentBinding;
import com.example.waitr.databinding.ActivityNotificationsBinding;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.GetUserInfoApi;
import com.example.waitr.model.LoginUserInfo;
import com.example.waitr.model.MyAppointment;
import com.example.waitr.utils.PrefManager;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

public class MyAppointmentActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityMyAppointmentBinding appointmentBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(MyAppointmentActivity.this);
        appointmentBinding = DataBindingUtil.setContentView(this,R.layout.activity_my_appointment);
        activity =  MyAppointmentActivity.this;
        initView();
    }

    private void initView() {
        appointmentBinding.imgBack.setOnClickListener(this);
        getMyAppointmentList();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
    }

    private void getMyAppointmentList(){
        Loader.show(activity);
        APIManager.getMyAppointmentApi(new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                MyAppointment myAppointment = (MyAppointment) modelclass;
                Loader.hide(activity);
                if(!myAppointment.getStatusCode().equals("200 OK")){
                    Toast.makeText(activity, myAppointment.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                myAppointment.getData().add(myAppointment.getData().get(0));
                myAppointment.getData().add(createData());
                myAppointment.getData().add(createData());
                myAppointment.getData().add(createData());
                createChangeColorList(myAppointment.getData());

                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private MyAppointment.Data createData(){
        MyAppointment.Data myAppointment = new MyAppointment.Data();
        myAppointment.setBookName("Company-2");
        myAppointment.setBookDate("2021-10-06");
        myAppointment.setBookID("16546");
        myAppointment.setPrice("16:30:00");
        return myAppointment;
    }

    private void createChangeColorList(List<MyAppointment.Data> appointmentList){
         int itemColorCount = 0;
         List<Integer> lightColorList = new ArrayList<>();
         List<Integer> darkColorList = new ArrayList<>();
        for(int i=0;i<appointmentList.size();i++){
            if(itemColorCount==0){
                lightColorList.add(R.color.skin_light);
                darkColorList.add(R.color.light_orange);
            }else if(itemColorCount==1){
                lightColorList.add(R.color.pink_light);
                darkColorList.add(R.color.light_red);
            }else if(itemColorCount==2){
                lightColorList.add(R.color.sky_light);
                darkColorList.add(R.color.light_blue);
            }
            itemColorCount++;
            if(itemColorCount==3){
                itemColorCount=0;
            }
        }
        MyAppointmentDataAdapter itemDataAdapter =
                new MyAppointmentDataAdapter(activity,appointmentList,lightColorList,darkColorList);
        appointmentBinding.recyclerAppointment.setAdapter(itemDataAdapter);
        itemDataAdapter.notifyDataSetChanged();
    }

}