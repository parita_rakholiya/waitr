package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.adapter.GetCategoryCompanyAdapter;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.databinding.ActivityFilterCompanyBinding;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.GetCategories;
import com.jaeger.library.StatusBarUtil;

import java.util.Calendar;

public class FilterCompanyActivity extends AppCompatActivity implements View.OnClickListener {
    
    private ActivityFilterCompanyBinding filterCompanyBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(FilterCompanyActivity.this);
        filterCompanyBinding = DataBindingUtil.setContentView(this,R.layout.activity_filter_company);
        activity = FilterCompanyActivity.this;
        initView();
    }

    private void initView() {
        filterCompanyBinding.imgBack.setOnClickListener(this);
        filterCompanyBinding.etDatePicker.setOnClickListener(this);
        getCategoryList();
    }

    private void getCategoryList(){
        Loader.show(activity);
        APIManager.getCategoryList(new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                Loader.hide(activity);
                GetCategories getCategories = (GetCategories) modelclass;
                if(!getCategories.getStatusCode().equals("200 OK")){
                    Toast.makeText(activity, getCategories.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                GetCategoryCompanyAdapter itemDataAdapter =
                        new GetCategoryCompanyAdapter(activity,getCategories.getData());
                filterCompanyBinding.recyclerResult.setAdapter(itemDataAdapter);
                itemDataAdapter.notifyDataSetChanged();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.img_back) {
            onBackPressed();
        }
        if(id==R.id.et_date_picker){
          openDatePickerDialog();
        }
    }

    private void openDatePickerDialog(){
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mdiDialog =new DatePickerDialog(activity,R.style.datepicker,new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String selectedDate = dayOfMonth+"/"+monthOfYear+"/"+year;
                filterCompanyBinding.etDatePicker.setText(selectedDate);

            }
        }, year, month, day);
        mdiDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        mdiDialog.show();
    }
}