package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.databinding.ActivityPrivacyPolicyBinding;
import com.example.waitr.databinding.ActivitySupportBinding;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.SendEmailPin;
import com.example.waitr.utils.Constants;
import com.jaeger.library.StatusBarUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class SupportActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivitySupportBinding supportBinding;
    private Activity activity;
    private String fullName,email,review;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(SupportActivity.this);
        supportBinding = DataBindingUtil.setContentView(this,R.layout.activity_support);
        activity =  SupportActivity.this;
        initView();
    }

    private void initView() {
        supportBinding.imgBack.setOnClickListener(this);
        supportBinding.llSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
        if(id==R.id.ll_submit){
            if(checkValidation()){
                callSubmitReviewApi("51452 opk");
            }
        }
    }

    private boolean checkValidation(){
        fullName = supportBinding.etFullName.getText().toString();
        email = supportBinding.etEmail.getText().toString();
        review = supportBinding.etWriteReview.getText().toString();
        if(fullName.isEmpty()){
            Toast.makeText(activity, "Please enter full name", Toast.LENGTH_SHORT).show();
            return false;
        }else if(TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(activity, "Please enter valid email", Toast.LENGTH_SHORT).show();
            return false;
        }else if(review.isEmpty()){
            Toast.makeText(activity, "Please write a review", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void callSubmitReviewApi(String ticketId){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.TICKET_ID,ticketId);
        hashMap.put(APIConstant.Parameter.EMAIL,email);
        hashMap.put(APIConstant.Parameter.FULL_NAME,fullName);
        hashMap.put(APIConstant.Parameter.REVIEW,review);

        Loader.show(activity);
        APIManager.submitReviewApi(hashMap,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                Loader.hide(activity);
                SendEmailPin sendEmailPin = (SendEmailPin) modelclass;
                if(!sendEmailPin.getStatus_code().equals("200 OK")){
                    Toast.makeText(activity, sendEmailPin.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                Toast.makeText(activity, sendEmailPin.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }



}