package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.databinding.ActivityHomeScreenBinding;
import com.example.waitr.fragment.home.CategoryWiseCompanyListFragment;
import com.example.waitr.fragment.FavoriteFragment;
import com.example.waitr.fragment.home.HomeFragment;
import com.example.waitr.fragment.MessagesFragment;
import com.example.waitr.fragment.ServicesFragment;
import com.example.waitr.utils.ViewControll;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

public class HomeScreenActivity extends AppCompatActivity implements View.OnClickListener{

    public ActivityHomeScreenBinding homeScreenBinding;
    private Activity activity;
    private boolean doubleBackToExitPressedOnce = false;
    public List<Fragment> fragmentList;
    private HomeFragment homeFragment;
    private MessagesFragment messagesFragment;
    private FavoriteFragment favoriteFragment;
    private ServicesFragment servicesFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(HomeScreenActivity.this);
        homeScreenBinding = DataBindingUtil.setContentView(this,R.layout.activity_home_screen);
        activity = HomeScreenActivity.this;
        initView();

    }

    private void initView() {
        homeScreenBinding.llNavHome.setOnClickListener(this);
        homeScreenBinding.llNavFavorite.setOnClickListener(this);
        homeScreenBinding.llNavMessage.setOnClickListener(this);
        homeScreenBinding.llNavServices.setOnClickListener(this);
        homeScreenBinding.imgProfile.setOnClickListener(this);
        setFragmentList();
        ViewControll.loadFragment(activity, homeFragment,"Home",fragmentList);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ViewControll.setLoginUserProfileImage(activity,homeScreenBinding.imgProfile);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_profile){
            startActivity(new Intent(activity,UserProfileActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
        if(id==R.id.ll_nav_home){
            changeTab(0);
            ViewControll.loadFragment(activity, homeFragment,"Home",fragmentList);

        }
        if(id==R.id.ll_nav_favorite){
            changeTab(2);
            ViewControll.loadFragment(activity, favoriteFragment,"Favorite",fragmentList);

        }
        if(id==R.id.ll_nav_message){
            changeTab(1);
            ViewControll.loadFragment(activity, messagesFragment,"Message",fragmentList);

        }
        if(id==R.id.ll_nav_services){
            changeTab(3);
            ViewControll.loadFragment(activity, servicesFragment,"Services",fragmentList);

        }

    }

    private void changeTab(int position){
        if(position==0) {
            homeScreenBinding.llNavHome.setBackgroundResource(R.drawable.bg_button_round_transparent);
            homeScreenBinding.llNavServices.setBackground(null);
            homeScreenBinding.llNavFavorite.setBackground(null);
            homeScreenBinding.llNavMessage.setBackground(null);
            homeScreenBinding.txtNavHome.setVisibility(View.VISIBLE);
            homeScreenBinding.txtNavMsg.setVisibility(View.GONE);
            homeScreenBinding.txtNavFav.setVisibility(View.GONE);
            homeScreenBinding.txtNavServices.setVisibility(View.GONE);
            homeScreenBinding.imgNavFav.setImageResource(R.drawable.ic_favorite);
            homeScreenBinding.imgNavServices.setImageResource(R.drawable.ic_services);
            homeScreenBinding.imgNavMsg.setImageResource(R.drawable.ic_message);
            homeScreenBinding.imgNavHome.setImageResource(R.drawable.ic_home_fill);
        }else if(position==1){
            homeScreenBinding.txtNavHome.setVisibility(View.GONE);
            homeScreenBinding.txtNavMsg.setVisibility(View.VISIBLE);
            homeScreenBinding.llNavMessage.setBackgroundResource(R.drawable.bg_button_round_transparent);
            homeScreenBinding.llNavServices.setBackground(null);
            homeScreenBinding.llNavFavorite.setBackground(null);
            homeScreenBinding.llNavHome.setBackground(null);
            homeScreenBinding.txtNavFav.setVisibility(View.GONE);
            homeScreenBinding.txtNavServices.setVisibility(View.GONE);
            homeScreenBinding.imgNavFav.setImageResource(R.drawable.ic_favorite);
            homeScreenBinding.imgNavServices.setImageResource(R.drawable.ic_services);
            homeScreenBinding.imgNavMsg.setImageResource(R.drawable.ic_nav_fill_message);
            homeScreenBinding.imgNavHome.setImageResource(R.drawable.ic_home_border);
        }else if(position==2){
            homeScreenBinding.txtNavHome.setVisibility(View.GONE);
            homeScreenBinding.txtNavMsg.setVisibility(View.GONE);
            homeScreenBinding.txtNavFav.setVisibility(View.VISIBLE);
            homeScreenBinding.llNavFavorite.setBackgroundResource(R.drawable.bg_button_round_transparent);
            homeScreenBinding.llNavServices.setBackground(null);
            homeScreenBinding.llNavHome.setBackground(null);
            homeScreenBinding.llNavMessage.setBackground(null);
            homeScreenBinding.txtNavServices.setVisibility(View.GONE);
            homeScreenBinding.imgNavFav.setImageResource(R.drawable.ic_nav_fill_favorite);
            homeScreenBinding.imgNavServices.setImageResource(R.drawable.ic_services);
            homeScreenBinding.imgNavMsg.setImageResource(R.drawable.ic_message);
            homeScreenBinding.imgNavHome.setImageResource(R.drawable.ic_home_border);
        }else if(position==3){
            homeScreenBinding.txtNavHome.setVisibility(View.GONE);
            homeScreenBinding.txtNavMsg.setVisibility(View.GONE);
            homeScreenBinding.txtNavFav.setVisibility(View.GONE);
            homeScreenBinding.txtNavServices.setVisibility(View.VISIBLE);
            homeScreenBinding.llNavServices.setBackgroundResource(R.drawable.bg_button_round_transparent);
            homeScreenBinding.llNavHome.setBackground(null);
            homeScreenBinding.llNavFavorite.setBackground(null);
            homeScreenBinding.llNavMessage.setBackground(null);
            homeScreenBinding.imgNavFav.setImageResource(R.drawable.ic_favorite);
            homeScreenBinding.imgNavServices.setImageResource(R.drawable.ic_nav_fill_services);
            homeScreenBinding.imgNavMsg.setImageResource(R.drawable.ic_message);
            homeScreenBinding.imgNavHome.setImageResource(R.drawable.ic_home_border);
        }

    }


    private void setFragmentList(){
        homeFragment = HomeFragment.newInstance();
        messagesFragment = MessagesFragment.newInstance();
        favoriteFragment = FavoriteFragment.newInstance();
        servicesFragment = ServicesFragment.newInstance();
        fragmentList =  new ArrayList<>();
        fragmentList.add(homeFragment);
        fragmentList.add(messagesFragment);
        fragmentList.add(favoriteFragment);
        fragmentList.add(servicesFragment);
    }

    @Override
    public void onBackPressed() {
        /*Fragment currentFrg = getSupportFragmentManager().findFragmentById(R.id.frgment_child);
        Fragment homeFrg = getSupportFragmentManager().findFragmentByTag("Home");
        assert homeFrg != null;
        if(homeFrg.isVisible()){
            assert currentFrg != null;
            if(currentFrg.isVisible() && currentFrg instanceof CategoryWiseCompanyListFragment){
                homeFrg.getChildFragmentManager().popBackStackImmediate();
                homeScreenBinding.toolbar.setVisibility(View.VISIBLE);
                return;
            }
        }*/

        Fragment homeFrg = getSupportFragmentManager().findFragmentByTag("Home");
        assert homeFrg != null;
        if(homeFrg.isVisible()) {
            if (recursivePopBackStack(getSupportFragmentManager())) {
                homeScreenBinding.toolbar.setVisibility(View.VISIBLE);
                return;
            }
        }

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    /**
     * Recursively look through nested fragments for a backstack entry to pop
     * @return: true if a pop was performed
     */
    public static boolean recursivePopBackStack(FragmentManager fragmentManager) {
        if (fragmentManager.getFragments() != null) {
            for (Fragment fragment : fragmentManager.getFragments()) {
                if (fragment != null && fragment.isVisible()) {
                    boolean popped = recursivePopBackStack(fragment.getChildFragmentManager());
                    if (popped) {
                        return true;
                    }
                }
            }
        }

        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
            return true;
        }

        return false;
    }

}