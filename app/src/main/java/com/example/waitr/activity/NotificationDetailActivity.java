package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.Glide;
import com.example.waitr.R;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.databinding.ActivityNotificationDetailBinding;
import com.example.waitr.interfaces.dialogPositiveClickListener;
import com.example.waitr.model.Notifications;
import com.example.waitr.utils.Constants;
import com.example.waitr.utils.ViewControll;
import com.jaeger.library.StatusBarUtil;

public class NotificationDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityNotificationDetailBinding detailBinding;
    private Activity activity;
    private Notifications.Data notificationItemData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(NotificationDetailActivity.this);
        detailBinding = DataBindingUtil.setContentView(this,R.layout.activity_notification_detail);
        activity =  NotificationDetailActivity.this;
        initView();
    }

    private void initView() {
        notificationItemData = (Notifications.Data) getIntent().getExtras().getSerializable(Constants.NOTIFICATIONS);
        detailBinding.imgBack.setOnClickListener(this);
        detailBinding.btnNo.setOnClickListener(this);
        detailBinding.btnYes.setOnClickListener(this);
        detailBinding.txtTitle.setText(notificationItemData.getComment());
        detailBinding.txtDescrip.setText(notificationItemData.getDescription());
        Glide.with(activity).load(APIConstant.IMAGE_BASE_URL+notificationItemData.getImage())
                .override(500,500)
                .placeholder(R.drawable.test_1)
                .into(detailBinding.imgNotification);
        Glide.with(activity).load(APIConstant.IMAGE_BASE_URL+notificationItemData.getImage())
                .override(500,500)
                .placeholder(R.drawable.test_1)
                .into(detailBinding.imgNotificationn);
        String formatDate = ViewControll.formatDate(notificationItemData.getCreatedDate(),
                "yyyy-MM-dd HH:mm:ss", "MMMM dd");
        detailBinding.txtDate.setText(formatDate);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
        if(id==R.id.btn_no){
            showBottomDailog(false);
        }
        if(id==R.id.btn_yes){
            showBottomDailog(true);
        }
    }

    private void showBottomDailog(boolean isClickYes){
        String dialogMsg = "" ;
        if(isClickYes){
            dialogMsg =  getString(R.string.booking_schadule_msg);
        }else {
            dialogMsg =  getString(R.string.booking_cancle_msg);
        }
        ViewControll.showBottomSheetDialog(activity,dialogMsg,
                getString(R.string.done),new dialogPositiveClickListener() {
                    @Override
                    public void positiveClick() {
                       finish();
                    }
                });
    }

}