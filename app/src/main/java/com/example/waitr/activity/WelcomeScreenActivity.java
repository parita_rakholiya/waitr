package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.adapter.WelcomeScreenAdapter;
import com.example.waitr.databinding.ActivityWelcomeScreenBinding;
import com.jaeger.library.StatusBarUtil;

public class WelcomeScreenActivity extends AppCompatActivity implements View.OnClickListener{

    private ActivityWelcomeScreenBinding welcomeScreenBinding;
    private Activity activity;
    private String[] titles;
    private String[] description;
    private int totalPageSize;
    private int[] images = {R.drawable.screen1,R.drawable.screen2,
            R.drawable.screen3,R.drawable.screen4,R.drawable.screen1};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(WelcomeScreenActivity.this);
        welcomeScreenBinding = DataBindingUtil.setContentView(this, R.layout.activity_welcome_screen);
        activity = WelcomeScreenActivity.this;
        titles = new String[]{getString(R.string.screen1_title), getString(R.string.screen2_title),
                getString(R.string.screen3_title)
                , getString(R.string.screen4_title)
                , getString(R.string.screen5_title)};
        description = new String[]{getString(R.string.screen1_descrip),
                getString(R.string.screen2_descrip),
                getString(R.string.screen2_descrip)
                , getString(R.string.screen2_descrip),
                getString(R.string.screen2_descrip)};

        initView();
    }


    private void initView(){

        WelcomeScreenAdapter adapter =  new WelcomeScreenAdapter(activity,
              titles,description,images);
        welcomeScreenBinding.viewpager.setAdapter(adapter);
        welcomeScreenBinding.wormDotsIndicator.setViewPager(welcomeScreenBinding.viewpager);

        welcomeScreenBinding.viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==0){
                    welcomeScreenBinding.llS2.setVisibility(View.VISIBLE);
                    welcomeScreenBinding.llS1.setVisibility(View.GONE);
                }else {
                    welcomeScreenBinding.llS2.setVisibility(View.GONE);
                    welcomeScreenBinding.llS1.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        welcomeScreenBinding.btnBack.setOnClickListener(this);
        welcomeScreenBinding.btnContinue.setOnClickListener(this);
        welcomeScreenBinding.btnGetStart.setOnClickListener(this);
        totalPageSize = welcomeScreenBinding.viewpager.getAdapter().getCount();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.btn_back){
            welcomeScreenBinding.viewpager.setCurrentItem(getCurrentPage()-1);
        }else if(id==R.id.btn_continue){
            if(getCurrentPage()==totalPageSize-1){
                startActivity(new Intent(activity,AskForSignupActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }else{
                welcomeScreenBinding.viewpager.setCurrentItem(getCurrentPage()+1);
            }
        }else if(id==R.id.btn_get_start){
            welcomeScreenBinding.viewpager.setCurrentItem(getCurrentPage()+1);
        }
    }

    private int getCurrentPage(){
        return welcomeScreenBinding.viewpager.getCurrentItem();
    }

}