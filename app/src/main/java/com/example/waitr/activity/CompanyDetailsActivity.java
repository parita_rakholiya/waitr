package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.waitr.R;
import com.example.waitr.databinding.ActivityCompanyDetailsBinding;
import com.jaeger.library.StatusBarUtil;

public class CompanyDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityCompanyDetailsBinding companyDetailsBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(CompanyDetailsActivity.this);
        companyDetailsBinding = DataBindingUtil.setContentView(this,R.layout.activity_company_details);
        activity = CompanyDetailsActivity.this;
        initView();
    }

    private void initView() {
        companyDetailsBinding.llShowAllReviews.setOnClickListener(this);
        companyDetailsBinding.llMakeAppointment.setOnClickListener(this);
        companyDetailsBinding.imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.ll_show_all_reviews){
            startActivity(new Intent(activity,AllReviewsActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
        if(id==R.id.ll_make_appointment){
            startActivity(new Intent(activity,MakeAppointmentActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
        if(id==R.id.img_back){
            onBackPressed();
        }
    }
}