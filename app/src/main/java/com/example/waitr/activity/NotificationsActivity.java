package com.example.waitr.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.waitr.BuildConfig;
import com.example.waitr.R;
import com.example.waitr.adapter.MyAppointmentDataAdapter;
import com.example.waitr.adapter.NotificationsDataAdapter;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.databinding.ActivityAboutUsBinding;
import com.example.waitr.databinding.ActivityNotificationsBinding;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.MyAppointment;
import com.example.waitr.model.Notifications;
import com.jaeger.library.StatusBarUtil;

public class NotificationsActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityNotificationsBinding notificationsBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(NotificationsActivity.this);
        notificationsBinding = DataBindingUtil.setContentView(this,R.layout.activity_notifications);
        activity =  NotificationsActivity.this;
        initView();
    }

    private void initView() {
        notificationsBinding.imgBack.setOnClickListener(this);
        getNotificationsList();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
    }

    private void getNotificationsList(){
        Loader.show(activity);
        APIManager.getNotificationsApi(new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                Notifications notifications = (Notifications) modelclass;
                Loader.hide(activity);
                if(!notifications.getStatusCode().equals("200 OK")){
                    Toast.makeText(activity, notifications.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                NotificationsDataAdapter itemDataAdapter =
                        new NotificationsDataAdapter(activity,notifications.getData());
                notificationsBinding.recyclerNotification.setAdapter(itemDataAdapter);
                itemDataAdapter.notifyDataSetChanged();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

}