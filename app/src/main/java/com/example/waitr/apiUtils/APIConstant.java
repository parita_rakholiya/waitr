package com.example.waitr.apiUtils;

public class APIConstant {

    public static final String errorNullResponse = "Something went wrong, Please try again!";

    public static final String BASE_URL = "http://waitr.foxyserver.com/api/";
    public static final String IMAGE_BASE_URL = "http://waitr.foxyserver.com/images/";


    public class API_ENDPOINTS {

        public static final String EMAIL_SEND_OTP = "send-pin.php";
        public static final String FORGOT_PASSWORD = "forgot-password.php";
        public static final String SET_OTP = "otp.php";
        public static final String LOGIN = "login.php";
        public static final String GET_USER_INFO = "get-userinfo.php";
        public static final String SIGNUP = "signup.php";
        public static final String UPDATE_USER_INFO = "update-user-info.php";
        public static final String SUPPORT = "support.php";
        public static final String MY_APPOINTMENT = "get-myoppointment.php";
        public static final String GET_NOTIFICATION = "get-notification.php";
        public static final String GET_BOOK_HISTORY = "get-bookhistry.php";
        public static final String GET_CATEGORY = "room-cleaning.php";
        public static final String POPULAR_SERVICES = "get-service.php";
    }



    public class Parameter{

        //signup
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
        public static final String NAME = "name";
        public static final String PHONE = "phone";
        public static final String ADDRESS = "address";
        public static final String HOUSE_NUMBER = "housNumber";
        public static final String ZIPCODE = "zipCode";
        public static final String CITY = "city";
        public static final String PROFILE_IMAGE = "profileImage";
        public static final String TYPE = "type";
        public static final String ID = "id";
        public static final String OTP = "otp";
        public static final String TICKET_ID = "ticketID";
        public static final String FULL_NAME = "fullname";
        public static final String REVIEW = "review";
    }

    public class KeyParameter{

        public static final String IS_LOGIN_DONE = "is_login_done";
        public static final String IS_FROM = "is_from";
        public static final String FROMSIGNIN = "signin";
        public static final String BACKTOLOGIN = "back_to_login";
        public static final String BACKFROMLOGOUT = "back_from_logout";
        public static final String FROM_REGISTER_USER = "register_user";
        public static final String FROM_FORGOT_PASSWORD = "forgot_password";
        public static final String FROM_CREATE_NEW = "from_new_password";
        public static final String FROM_RESET_PASSWORD = "reset_password";
        public static final String USER_ID = "user_id";
        public static final String IS_SOCIAL_LOGIN = "is_social_login";
    }




}
