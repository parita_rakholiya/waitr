package com.example.waitr.apiUtils;


import android.util.Log;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APICaller {

    private static String res =  null;

    public static APIInterface getRetrofitInstance() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(3, TimeUnit.MINUTES)
                .readTimeout(3, TimeUnit.MINUTES)
                .writeTimeout(3, TimeUnit.MINUTES)
                .build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(baseURL()).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        return  retrofit.create(APIInterface.class);
    }


    public static <T> Class<T> getRequest(String url, HashMap<String, String> params, final Class<T> modelclass,final APICallBack apiCallBack) {
        getRetrofitInstance().requestWithGet(url,new HashMap<String, String>(),params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                if(response.body()==null){

                    return;
                }

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);
                objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
                objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true);

                try {

                    T  model  = objectMapper.readValue(response.body().string(),modelclass);
                    Log.d("response===",response.body().string());
                    apiCallBack.onSuccess(model);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                apiCallBack.onFailure();
            }
        });

        return null;
    }

    public static <T> Class<T> postRequest(String url, HashMap<String, String> params, final Class<T> modelclass, final APICallBack apiCallBack) {

        getRetrofitInstance().requestWithPost(url,new HashMap<String, String>(),params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.body()==null){
                    return;
                }

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);
                objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
                objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true);

                try {

                    T  model  = objectMapper.readValue(response.body().string(),modelclass);
                    Log.d("response===",response.body().string());
                    apiCallBack.onSuccess(model);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                apiCallBack.onFailure();
            }
        });

        return null;
    }

    public static <T> Class<T> postMultipartRequest(String url, HashMap<String, RequestBody> params, MultipartBody.Part image,
                                                    final Class<T> modelclass, final APICallBack apiCallBack) {

        getRetrofitInstance().requestWithPostImage(url,new HashMap<String, String>(),params,image).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.body()==null){
                    return;
                }

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);
                objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
                objectMapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true);

                try {

                    T  model  = objectMapper.readValue(response.body().string(),modelclass);
                    Log.d("response===",response.body().string());
                    apiCallBack.onSuccess(model);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                apiCallBack.onFailure();
            }
        });

        return null;
    }



    public interface APICallBack{

        <T> Class<T> onSuccess(T modelclass);
        void onFailure();
    }

    public interface APICallBackForList{

        <T> List<T> onSuccess(List<T> tList);
        void onFailure();
    }



    public static String baseURL() {
        return APIConstant.BASE_URL;
    }

}
