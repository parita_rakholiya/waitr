package com.example.waitr.apiUtils;


import com.example.waitr.model.BookingHistory;
import com.example.waitr.model.GetCategories;
import com.example.waitr.model.GetPopularServices;
import com.example.waitr.model.LoginApiModel;
import com.example.waitr.model.LoginUserInfo;
import com.example.waitr.model.MyAppointment;
import com.example.waitr.model.Notifications;
import com.example.waitr.model.SendEmailPin;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class APIManager {

  public static void sendEmailOtpApi(String email,String password,APICaller.APICallBack apiCallBack){

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.EMAIL,email);
        hashMap.put(APIConstant.Parameter.PASSWORD,password);

        APICaller.postRequest(APIConstant.API_ENDPOINTS.EMAIL_SEND_OTP,hashMap, SendEmailPin.class, apiCallBack);
    }

    public static void forgotPasswordApi(String email,String password,APICaller.APICallBack apiCallBack){

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.EMAIL,email);
        hashMap.put(APIConstant.Parameter.PASSWORD,password);

        APICaller.postRequest(APIConstant.API_ENDPOINTS.FORGOT_PASSWORD,hashMap, SendEmailPin.class, apiCallBack);
    }

    public static void loginApi(String email,String password,APICaller.APICallBack apiCallBack){

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.EMAIL,email);
        hashMap.put(APIConstant.Parameter.PASSWORD,password);

        APICaller.postRequest(APIConstant.API_ENDPOINTS.LOGIN,hashMap, LoginApiModel.class, apiCallBack);
    }

    public static void getLoginUserInfo(String id,APICaller.APICallBack apiCallBack){

        APICaller.getRequest(APIConstant.API_ENDPOINTS.GET_USER_INFO+"?id="+id,new HashMap<>(), LoginUserInfo.class, apiCallBack);
    }

    public static void signUpApi(HashMap<String, String> hashMap,APICaller.APICallBack apiCallBack){

        APICaller.postRequest(APIConstant.API_ENDPOINTS.SIGNUP,hashMap, SendEmailPin.class, apiCallBack);
    }

    public static void submitReviewApi(HashMap<String, String> hashMap,APICaller.APICallBack apiCallBack){

        APICaller.postRequest(APIConstant.API_ENDPOINTS.SUPPORT,hashMap, SendEmailPin.class, apiCallBack);
    }

    public static void updateUserInfoApi(HashMap<String, RequestBody> hashMap, MultipartBody.Part image,APICaller.APICallBack apiCallBack){

        APICaller.postMultipartRequest(APIConstant.API_ENDPOINTS.UPDATE_USER_INFO,hashMap,image, SendEmailPin.class, apiCallBack);
    }

    public static void setEmailOtpApi(String otp,APICaller.APICallBack apiCallBack){

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.OTP,otp);

        APICaller.postRequest(APIConstant.API_ENDPOINTS.SET_OTP,hashMap, SendEmailPin.class, apiCallBack);
    }

    public static void getMyAppointmentApi(APICaller.APICallBack apiCallBack){

        APICaller.getRequest(APIConstant.API_ENDPOINTS.MY_APPOINTMENT,new HashMap<>(), MyAppointment.class, apiCallBack);
  }

    public static void getNotificationsApi(APICaller.APICallBack apiCallBack){

        APICaller.getRequest(APIConstant.API_ENDPOINTS.GET_NOTIFICATION,new HashMap<>(), Notifications.class, apiCallBack);
    }

    public static void getBookingHistoryApi(APICaller.APICallBack apiCallBack){

        APICaller.getRequest(APIConstant.API_ENDPOINTS.GET_BOOK_HISTORY,new HashMap<>(), BookingHistory.class, apiCallBack);
    }

    public static void getCategoryList(APICaller.APICallBack apiCallBack){

        APICaller.getRequest(APIConstant.API_ENDPOINTS.GET_CATEGORY,new HashMap<>(), GetCategories.class, apiCallBack);
    }


    public static void getPopularServiceList(APICaller.APICallBack apiCallBack){

        APICaller.getRequest(APIConstant.API_ENDPOINTS.POPULAR_SERVICES,new HashMap<>(), GetPopularServices.class, apiCallBack);
    }


}
