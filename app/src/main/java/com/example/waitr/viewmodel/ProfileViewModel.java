package com.example.waitr.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.waitr.R;

import java.util.ArrayList;
import java.util.List;

public class ProfileViewModel extends ViewModel {

    private MutableLiveData<List<ProfileScreenItemData>> mProfileItemData;

    public ProfileViewModel() {
        mProfileItemData = new MutableLiveData<>();
        setProfileScreenLItemList();
    }

    public LiveData<List<ProfileScreenItemData>> getProfileItemData() {
        return mProfileItemData;
    }


    public void setProfileScreenLItemList(){
        List<ProfileScreenItemData> profileScreenItemData =  new ArrayList<>();
        ProfileScreenItemData itemData = new ProfileScreenItemData(R.drawable.ic_profile_user,"Personal Info");
        profileScreenItemData.add(itemData);
        itemData  = new ProfileScreenItemData(R.drawable.ic_setting,"Setting");
        profileScreenItemData.add(itemData);
        itemData  = new ProfileScreenItemData(R.drawable.ic_support,"Support");
        profileScreenItemData.add(itemData);
        itemData  = new ProfileScreenItemData(R.drawable.ic_privacy_policy,"Privacy & Policy");
        profileScreenItemData.add(itemData);
        itemData  = new ProfileScreenItemData(R.drawable.ic_notification,"Notifications");
        profileScreenItemData.add(itemData);
        itemData  = new ProfileScreenItemData(R.drawable.ic_booking_history,"Booking History");
        profileScreenItemData.add(itemData);
        itemData  = new ProfileScreenItemData(R.drawable.ic_appointment,"Appointments");
        profileScreenItemData.add(itemData);
        itemData  = new ProfileScreenItemData(R.drawable.ic_about_us,"About Us");
        profileScreenItemData.add(itemData);
        itemData  = new ProfileScreenItemData(R.drawable.ic_logout,"Log Out");
        profileScreenItemData.add(itemData);
        mProfileItemData.setValue(profileScreenItemData);
    }

}