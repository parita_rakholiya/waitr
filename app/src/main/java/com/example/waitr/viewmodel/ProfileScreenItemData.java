package com.example.waitr.viewmodel;

public class ProfileScreenItemData {

    private int imgIcon;
    private String itemTitle;

    public ProfileScreenItemData(int imgIcon, String itemTitle) {
        this.imgIcon = imgIcon;
        this.itemTitle = itemTitle;
    }

    public int getImgIcon() {
        return imgIcon;
    }

    public void setImgIcon(int imgIcon) {
        this.imgIcon = imgIcon;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }
}
