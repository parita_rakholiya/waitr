package com.example.waitr.fragment.home;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.waitr.adapter.GetCategoryAdapter;
import com.example.waitr.adapter.GetPolularServicesAdapter;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.databinding.FragmentHomeBinding;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.GetCategories;
import com.example.waitr.model.GetPopularServices;
import com.example.waitr.utils.ViewControll;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    private FragmentHomeBinding homeBinding;
    private Activity activity;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        homeBinding = FragmentHomeBinding.inflate(inflater, container, false);
        activity = getActivity();
        return homeBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewControll.loadSubFragment(activity,HomeMainFragment.newInstance(),
                "HomeMainFragment",false);
    }

}