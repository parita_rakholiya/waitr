package com.example.waitr.fragment.home;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.adapter.GetCategoryAdapter;
import com.example.waitr.adapter.GetPolularServicesAdapter;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.databinding.FragmentHomeBinding;
import com.example.waitr.databinding.FragmentHomeMainBinding;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.GetCategories;
import com.example.waitr.model.GetPopularServices;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeMainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeMainFragment extends Fragment {

    private FragmentHomeMainBinding homeMainBinding;
    private Activity activity;

    public HomeMainFragment() {
        // Required empty public constructor
    }

    public static HomeMainFragment newInstance() {
        HomeMainFragment fragment = new HomeMainFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        homeMainBinding = FragmentHomeMainBinding.inflate(inflater, container, false);
        activity = getActivity();
        return homeMainBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getCategoryList();
    }

    private void getCategoryList(){
        Loader.show(activity);
        APIManager.getCategoryList(new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                GetCategories getCategories = (GetCategories) modelclass;
                if(!getCategories.getStatusCode().equals("200 OK")){
                    Loader.hide(activity);
                    Toast.makeText(activity, getCategories.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                GetCategoryAdapter itemDataAdapter =
                        new GetCategoryAdapter(activity,getCategories.getData());
                homeMainBinding.recyclerCategory.setAdapter(itemDataAdapter);
                itemDataAdapter.notifyDataSetChanged();
                getPopularServiceList();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private void getPopularServiceList(){
        APIManager.getPopularServiceList(new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                GetPopularServices popularServices = (GetPopularServices) modelclass;
                Loader.hide(activity);
                if(!popularServices.getStatusCode().equals("200 OK")){
                    Toast.makeText(activity, popularServices.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                GetPolularServicesAdapter itemDataAdapter =
                        new GetPolularServicesAdapter(activity,popularServices.getData());
                homeMainBinding.recyclerPopularService.setAdapter(itemDataAdapter);
                itemDataAdapter.notifyDataSetChanged();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }
}