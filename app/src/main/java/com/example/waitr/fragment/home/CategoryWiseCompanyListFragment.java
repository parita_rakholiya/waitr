package com.example.waitr.fragment.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.waitr.R;
import com.example.waitr.activity.FilterCompanyActivity;
import com.example.waitr.activity.HomeScreenActivity;
import com.example.waitr.adapter.GetCategoryCompanyAdapter;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.databinding.FragmentCategoryWiseCompanyListBinding;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.GetCategories;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CategoryWiseCompanyListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoryWiseCompanyListFragment extends Fragment implements View.OnClickListener {

    private FragmentCategoryWiseCompanyListBinding categoryWiseCompanyListBinding;
    private Activity activity;
    private static GetCategories.Data categoryData;

    public CategoryWiseCompanyListFragment() {
        // Required empty public constructor
    }

    public static CategoryWiseCompanyListFragment newInstance(GetCategories.Data data) {
        CategoryWiseCompanyListFragment fragment = new CategoryWiseCompanyListFragment();
        categoryData = data;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        categoryWiseCompanyListBinding = FragmentCategoryWiseCompanyListBinding.inflate(inflater, container, false);
        activity = getActivity();
        return categoryWiseCompanyListBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        categoryWiseCompanyListBinding.imgBack.setOnClickListener(this);
        categoryWiseCompanyListBinding.imgFilter.setOnClickListener(this);
        categoryWiseCompanyListBinding.txtToolbarTitle.setText(categoryData.getCategory());
        getCategoryList();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            ((HomeScreenActivity)activity).onBackPressed();
        }
        if(id==R.id.img_filter){
            startActivity(new Intent(activity, FilterCompanyActivity.class)
            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    private void getCategoryList(){
        Loader.show(activity);
        APIManager.getCategoryList(new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                Loader.hide(activity);
                GetCategories getCategories = (GetCategories) modelclass;
                if(!getCategories.getStatusCode().equals("200 OK")){
                    Toast.makeText(activity, getCategories.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                GetCategoryCompanyAdapter itemDataAdapter =
                        new GetCategoryCompanyAdapter(activity,getCategories.getData());
                categoryWiseCompanyListBinding.recyclerCompanyList.setAdapter(itemDataAdapter);
                itemDataAdapter.notifyDataSetChanged();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    public void backFromFragment(){
        getChildFragmentManager().popBackStackImmediate();
    }

}