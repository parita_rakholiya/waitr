package com.example.waitr.app;

import android.app.Application;
import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.multidex.MultiDex;


public class MyAPP extends Application {

    private static Context context;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onCreate() {
        super.onCreate();

        MyAPP.context = getApplicationContext();
        MultiDex.install(context);
    }

    public static Context getAppContext() {


        return MyAPP.context;
    }

    @Override
    public void attachBaseContext(Context base) {

        super.attachBaseContext(base);

    }

}
