package com.example.waitr.utils;

import com.example.waitr.R;
import com.example.waitr.model.UserProfileData;
import com.example.waitr.viewmodel.ProfileScreenItemData;

import java.util.ArrayList;
import java.util.List;

public class Constants {

    public static final String COMPANY_DETAILS = "company_details";
    public static boolean isRegisterClick = false;
    public static boolean isRetrivePasswordClick = false;
    public static String registeredEmail = "";
    public static UserProfileData loginUserProfileData = null;
    public static String PERSONAL_INFO = "Personal Info";
    public static String SETTINGS = "Setting";
    public static String SUPPORT = "Support";
    public static String PRIVACY_POLICY = "Privacy & Policy";
    public static String NOTIFICATIONS = "Notifications";
    public static String BOOKING_HISTORY = "Booking History";
    public static String APPOINTMENT = "Appointments";
    public static String ABOUT_US = "About Us";
    public static String LOGOUT = "Log Out";
}

