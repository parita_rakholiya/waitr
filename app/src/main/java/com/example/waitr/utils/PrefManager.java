package com.example.waitr.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.waitr.app.MyAPP;
import com.example.waitr.model.LoginUserData;
import com.example.waitr.model.LoginUserInfo;
import com.google.gson.Gson;

public class PrefManager {

    public static void saveLoginUser(LoginUserInfo user) {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("waitr", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(user);
        sharedPreferencesEditor.putString("SaveLoginUserDetails", serializedObject);
        sharedPreferencesEditor.apply();
    }

    public static Boolean isUserExist() {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("waitr", Context.MODE_PRIVATE);
        return sharedPreferences.contains("SaveLoginUserDetails");
    }


    public static LoginUserInfo loginUser() {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("waitr", 0);
        if (sharedPreferences.contains("SaveLoginUserDetails")) {
            final Gson gson = new Gson();

            return gson.fromJson(sharedPreferences.getString("SaveLoginUserDetails", ""), LoginUserInfo.class);
        }
        return null;
    }


    public static void deleteuser(){
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("waitr", 0);
        sharedPreferences.edit().clear().apply();

    }

    public static void putString(String key, String value) {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("waitr", 0);
        checkForNullKey(key);
        checkForNullValue(value);
        sharedPreferences.edit().putString(key, value).apply();
    }

    public static String getString(String key,String defaultvalue) {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("waitr", 0);
        return sharedPreferences.getString(key, defaultvalue);
    }

    public static void putBoolean(String key, boolean value) {

        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("waitr", 0);
        checkForNullKey(key);
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public static boolean getBoolean(String key)
    {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("waitr", 0);
        return sharedPreferences.getBoolean(key, false);

    }



    public static void checkForNullKey(String key) {
        if (key == null) {
            throw new NullPointerException();
        }
    }


    public static void checkForNullValue(String value) {
        if (value == null) {
            throw new NullPointerException();
        }
    }

}
