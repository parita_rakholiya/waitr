package com.example.waitr.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.example.waitr.R;
import com.example.waitr.activity.CheckMailBoxActivity;
import com.example.waitr.activity.CreateFullNameActivity;
import com.example.waitr.apiUtils.APICaller;
import com.example.waitr.apiUtils.APIConstant;
import com.example.waitr.apiUtils.APIManager;
import com.example.waitr.interfaces.dialogPositiveClickListener;
import com.example.waitr.loader.Loader;
import com.example.waitr.model.LoginApiModel;
import com.example.waitr.model.LoginUserData;
import com.example.waitr.model.LoginUserInfo;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Logger;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class ViewControll {

    private static final String TAG = "ViewControll";

    public static void showAlertDialog(Activity activity, String title, String message, dialogPositiveClickListener dialogPositiveClickListener){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.CustomAlertDialog);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogPositiveClickListener.positiveClick();
                dialog.dismiss();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    public static void showBottomSheetDialog(Activity activity,String txt_msg,String btnText,
                                             dialogPositiveClickListener positiveClickListener) {

        final BottomSheetDialog bottomSheetDialog =
                new BottomSheetDialog(activity,R.style.CustomBottomSheetDialog);
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_otp_verified);
        Button btnContinue = bottomSheetDialog.findViewById(R.id.btn_continue);
        TextView txtMsg = bottomSheetDialog.findViewById(R.id.txt_msg);
        txtMsg.setText(txt_msg);
        btnContinue.setText(btnText);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                positiveClickListener.positiveClick();
            }
        });
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.show();
    }

    public static void setLoginUserProfileImage(Activity activity, CircleImageView imgProfile){
        if(PrefManager.isUserExist()){

            LoginUserInfo loginUserInfo = PrefManager.loginUser();
            if(!loginUserInfo.getProfileImage().isEmpty()){
                String photoUrl =loginUserInfo.getProfileImage();
                if(!PrefManager.getBoolean(APIConstant.KeyParameter.IS_SOCIAL_LOGIN)){
                    photoUrl = APIConstant.IMAGE_BASE_URL+photoUrl;
                }

                        Glide.with(activity).load(photoUrl).override(500,500)
                                .placeholder(R.drawable.ic_profile_placeholder)
                        .error(R.drawable.ic_profile_placeholder)
                        .into(imgProfile);
            }else {
                imgProfile.setImageResource(R.drawable.ic_profile_placeholder);
            }


        }
    }

    public static void loadFragment(Activity activity, Fragment fragment, String tag, List<Fragment> fragmentList) {
        int index = getFragmentIndex(fragment,fragmentList);
        FragmentTransaction transaction = ((AppCompatActivity)activity).getSupportFragmentManager().beginTransaction();
        if (fragment.isAdded()) { // if the fragment is already in container
            transaction.show(fragment);
            Log.d("fragment==","show-"+fragment);
        } else { // fragment needs to be added to frame container
            transaction.add(R.id.frgment_main, fragment,tag);
//            transaction.addToBackStack(tag);
            Log.d("fragment==","add-"+fragment);
        }
        // hiding the other fragments
        for (int i = 0; i < fragmentList.size(); i++) {
            if (fragmentList.get(i).isAdded() && i != index) {
                transaction.hide(fragmentList.get(i));
                Log.d("fragment==","hide-"+fragmentList.get(i));
            }
        }
        transaction.commit();
    }

    public static void loadSubFragment(Activity activity, Fragment fragment, String tag,boolean isBackstack) {
        FragmentTransaction transaction = ((AppCompatActivity)activity).getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frgment_child, fragment,tag);
        if(isBackstack){
            transaction.addToBackStack(tag);
        }
        Log.d("fragment==","add-"+fragment);
        transaction.commit();
    }



    private static int getFragmentIndex(Fragment fragment, List<Fragment> fragmentList) {
        int index = -1;
        for (int i = 0; i < fragmentList.size(); i++) {
            if (fragment.hashCode() == fragmentList.get(i).hashCode()){
                return i;
            }
        }
        return index;
    }

    public static boolean isStoragePermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    public static boolean isCameraPermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity.checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    public static RequestBody convertStringToRequestBody(String itemValue){
        return RequestBody.create(itemValue,MediaType.parse("text/plain"));
    }

    public static String formatDate(String dateToFormat, String inputFormat, String outputFormat) {
        try {
            Log.e("DATE", "Input Date Date is " + dateToFormat);

            String convertedDate = new SimpleDateFormat(outputFormat)
                    .format(new SimpleDateFormat(inputFormat)
                            .parse(dateToFormat));

            Log.e("DATE", "Output Date is " + convertedDate);

            //Update Date
            return convertedDate;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;

    }

    public static String setDateWiseHeader(String date,String pattern) {
        DateTime dateTime = DateTimeFormat.forPattern(pattern).parseDateTime(date);
        DateTime today = new DateTime();
        DateTime yesterday = today.minusDays(1);
        DateTime twodaysago = today.minusDays(2);
        DateTime threedaysago = today.minusDays(3);
        DateTime fourdaysago = today.minusDays(4);
        DateTime fivedaysago = today.minusDays(5);
        DateTime sixdaysago = today.minusDays(6);
        DateTime savendaysago = today.minusDays(7);

        if (dateTime.toLocalDate().equals(today.toLocalDate())) {
            return "Today ";
        } else if (dateTime.toLocalDate().equals(yesterday.toLocalDate())) {
            return "Yesterday ";
        } else if (dateTime.toLocalDate().equals(twodaysago.toLocalDate())) {
            return "2 days ago ";
        }else if (dateTime.toLocalDate().equals(threedaysago.toLocalDate())) {
            return "3 days ago ";
        }else if (dateTime.toLocalDate().equals(fourdaysago.toLocalDate())) {
            return "4 days ago ";
        }else if (dateTime.toLocalDate().equals(fivedaysago.toLocalDate())) {
            return "5 days ago ";
        }else if (dateTime.toLocalDate().equals(sixdaysago.toLocalDate())) {
            return "6 days ago ";
        }else if (dateTime.toLocalDate().equals(savendaysago.toLocalDate())) {
            return "1 week ago ";
        } else {
            String formatDate = formatDate(date,
                    pattern, "dd MMMM yyyy");
            return formatDate;
        }
    }

}
